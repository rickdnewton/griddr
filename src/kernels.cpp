// #include <fstream>
#include <iostream>
#include <cmath>
#include <stdexcept>
// #include <algorithm>
// #include <cstring>
// #include <string>

#include "dynamic_array.h"
#include "particle_data.h"
#include "three_d_grid.h"
#include "globals.h"
#include "proto.h"
#include "kernels.h"
#include "default_error.h"

using namespace std;

/**
    \file kernels.cpp
    An implementation file containing the function definitions for all available 
    SPH smoothing kernels
*/

//! Control function which initialises the chosen SPH smoothing kernel and sets function pointers
/*!
  \param form an integer representing the desired smoothing kernel
*/
void kernel_init(int form) {

  switch(form) {
  case 0:
    kernel_eval = CS_kernel_eval;
    dw_kernel_eval = CS_dw_kernel_eval;
#ifndef THREE_DIMS
    CSvar::k_w = 5.0 / 7.0 * 8.0 / pi;
#else
    CSvar::k_w = 8.0 / pi;
#endif
    break;
  case 1:
    kernel_eval = WC2_kernel_eval;
    dw_kernel_eval = WC2_dw_kernel_eval;
#ifndef THREE_DIMS
    WCvar::k_w = 7.0 / pi;
    WCvar::k_dw = 20 * 7.0 / pi;
#else
    WCvar::k_w = 21.0 / (2 * pi);
    WCvar::k_dw = 20 * 21.0 / (2 * pi);
#endif
    break;
  case 2:
    kernel_eval = WC4_kernel_eval;
    dw_kernel_eval = WC4_dw_kernel_eval;
#ifndef THREE_DIMS
    WCvar::k_w = 9.0 / pi;
    WCvar::k_dw = 56 * 9.0 / pi;
#else
    WCvar::k_w = 495.0 / (32 * pi);
    WCvar::k_dw = 56 * 495.0 / (32 * pi);
#endif
    break;
  case 3:
    kernel_eval = WC6_kernel_eval;
    dw_kernel_eval = WC6_dw_kernel_eval;
#ifndef THREE_DIMS
    WCvar::k_w = 78.0 / (7 * pi);
    WCvar::k_dw = 78.0 / (7 * pi);
#else
    WCvar::k_w = 1365.0 / (64 * pi);
    WCvar::k_dw = 1365.0 / (64 * pi);
#endif
    break;
  case 4:
#ifndef THREE_DIMS
    kernel_eval = HOCT_kernel_eval_2D;
    dw_kernel_eval = HOCT_dw_kernel_eval_2D;
    HOCT_kernel_init_2D();
#else
    kernel_eval = HOCT_kernel_eval_3D;
    dw_kernel_eval = HOCT_dw_kernel_eval_3D;
    HOCT_kernel_init_3D();
#endif
    break;
  default:
    throw DefaultError("ERROR: Invalid kernel specification");
  }

}

#ifndef THREE_DIMS
//! Inline function returning h^2 or h^3 depending on whether 2D or 3D
inline double kernel_h3(double& h) {
  return h * h;// * (zLim_g[1] - zLim_g[0]);
}
//! Inline function returning the  2D or 3D kernel normalisation
inline double kernel_norm(double& h3) {
  return h3 * pi;
}
#else
//! Inline function returning h^2 or h^3 depending on whether 2D or 3D
inline double kernel_h3(double& h) {
  return h * h * h;
}
//! Inline function returning the  2D or 3D kernel normalisation
inline double kernel_norm(double& h3) {
  return h3 * 4.0 / 3.0 * pi;
}
#endif

//! Function returning the value of the cubic spline kernel at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double CS_kernel_eval(double r, double h) {
  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);

  double k = CSvar::k_w / h3;
  double u = r / h;
  double u2 = u*u;
  double u3 = u2*u;

  double w = 0;

  if (u < 0.5) {
    w = 1.0  -  6.0 * u2 + 6.0 * u3;
  } else if (u <= 1.0) {
    w = 2.0 * (1.0  -  3.0*u + 3.0* u2  -  u3);
  }

  w *= k;
  return w;
}

//! Function returning the value of the cubic spline kernel gradient at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double CS_dw_kernel_eval(double r, double h) {
  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);

  double k = CSvar::k_w / h3;
  double u = r / h;
  double u2 = u*u;
  // double u3 = u2*u;


  double w = 0;

  if (u < 0.5) {
    w = 1.0 / h * u * (18 * u  -  12);
  } else if (u <= 1.0) {
    w =  - 6 * 1.0 / h * (1.0 + u2  -  2 * u);
  }

  w *= k;
  return w;
}

//! Function returning the value of the second order Wendland kernel at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double WC2_kernel_eval(double r, double h) {
  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);

  const double k = WCvar::k_w;
  double u = r / h;

  double w = pow(1.0  -  u, 4) * (1.0 + 4.0*u) / h3;

  w *= k;
  return w;
}

//! Function returning the value of the second order Wendland kernel gradient at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double WC2_dw_kernel_eval(double r, double h) {
  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);
  double h4 = h3 * h;

  const double k = WCvar::k_dw;
  double u = r / h;

  double w =  -  pow(1.0  -  u, 3) * u / h4;

  w *= k;
  return w;
}

//! Function returning the value of the fourth order Wendland kernel at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double WC4_kernel_eval(double r, double h) {
  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);

  const double k = WCvar::k_w;
  double u = r / h;

  double w = pow(1.0  -  u, 6) * (1.0 + 6.0*u + 35.0/3.0*u*u) / h3;

  w *= k;
  return w;
}

//! Function returning the value of the fourth order Wendland kernel gradient at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double WC4_dw_kernel_eval(double r, double h) {
  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);
  double h4 = h3 * h;

  const double k = WCvar::k_dw;
  double u = r / h;

  double w =  -  pow(1.0  -  u, 5) * u * (1.0 + 5.0*u) / h4;

  w *= k;
  return w;
}

//! Function returning the value of the sixth order Wendland kernel at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double WC6_kernel_eval(double r, double h) {
  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);

  const double k = WCvar::k_w;
  double u = r / h;
  double u2 = u*u;
  double u3 = u2*u;

  double w = pow(1.0  -  u, 8) * (1.0 + 8.0*u + 25.0*u2 + 32.0*u3) / h3;

  w *= k;
  return w;
}

//! Function returning the value of the sixth order Wendland kernel gradient at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double WC6_dw_kernel_eval(double r, double h) {
  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);
  double h4 = h3 * h;

  const double k = WCvar::k_dw;
  double u = r / h;
  double u2 = u*u;
  double u3 = u2*u;

  double w =  -  pow(1.0  -  u, 8) * (22.0*u + 154.0*u2 + 352.0*u3) / h4;

  w *= k;
  return w;
}

//! Function returning the value of the 3D higher order OCT kernel at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double HOCT_kernel_eval_3D(double r, double h) {
  using HOCTvar::normfac;
  using HOCTvar::alp;
  using HOCTvar::bet;
  using HOCTvar::nn;
  using HOCTvar::Afac;
  using HOCTvar::Bfac;
  using HOCTvar::Pfac;
  using HOCTvar::Qfac;
  using HOCTvar::kap;

  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);

  double w, normkern3;
  double u = r / h;

  normkern3 = normfac / h3;
  if(u < kap) {
    w = normkern3 * (Pfac * u + Qfac);
  } else if(u < bet) {
    w = normkern3 * (pow((1.0  -  u),nn) + Afac * pow((alp - u),nn) + Bfac * pow((bet - u),nn));
  } else if(u < alp) {
    w = normkern3 * (pow((1.0  -  u),nn) + Afac * pow((alp - u),nn));
  } else {
    w = normkern3 * pow((1.0  -  u),nn);
  }

  return w;
}

//! Function returning the value of the 3D higher order OCT kernel gradient at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double HOCT_dw_kernel_eval_3D(double r, double h) {
  using HOCTvar::normfac;
  using HOCTvar::alp;
  using HOCTvar::bet;
  using HOCTvar::nn;
  using HOCTvar::Afac;
  using HOCTvar::Bfac;
  using HOCTvar::Pfac;
  using HOCTvar::Qfac;
  using HOCTvar::kap;

  double h3 = kernel_h3(h);
  // double norm = kernel_norm(h3);

  double w, normkern4;

  double h4 = h3 * h;
  double u = r / h;

  normkern4 = normfac / h4;
  if(u < kap) {
    w = normkern4 * Pfac;
  } else if(u < bet) {
    w =  - normkern4 * (nn * pow((1.0  -  u),nn - 1) + nn * Afac * pow((alp - u),nn - 1) + nn * Bfac * pow((bet - u),nn - 1));
  } else if(u < alp) {
    w =  - normkern4 * (nn * pow((1.0  -  u),nn - 1) + nn * Afac * pow((alp - u),nn - 1));
  } else {
    w =  - normkern4 * nn * pow((1.0  -  u),nn - 1);
  }

  return w;
}

//! Function to numerically evaluate and initialise the normalisations of the 3D higher order OCT kernel
void HOCT_kernel_init_3D() {
  using HOCTvar::normfac;
  using HOCTvar::alp;
  using HOCTvar::bet;
  using HOCTvar::nn;
  using HOCTvar::Afac;
  using HOCTvar::Bfac;
  using HOCTvar::Nsamp;
  using HOCTvar::Pfac;
  using HOCTvar::Qfac;
  using HOCTvar::kap;

  kap = 0;
  double prev_kap;
  double errTol = 1e-6;
  double bc;
  double kstep = 1;
  double oside, side = 1;
  do {
    bc = pow(1 - kap, nn - 2) + Afac * pow(alp - kap, nn - 2) + Bfac * pow(bet - kap, nn - 2);

    prev_kap = kap;
    oside = side;
    if(bc > 0) side = 1;
    else side = -1;


    if(oside!=side) kstep /= 2;
    if(bc > 0)
      kap -= kstep;
    else
      kap += kstep;
  } while(fabs(bc) > errTol);


  Pfac =  -  nn * pow(1  -  kap, nn  -  1)  -  nn * Afac * pow(alp  -  kap, nn  -  1)  -  nn * Bfac * pow(bet  -  kap, nn  -  1);
  Qfac = pow(1  -  kap, nn) + Afac * pow(alp  -  kap, nn) + Bfac * pow(bet  -  kap, nn)  -  Pfac * kap;

  normfac = 1.0;// / (pi * 4.0 / 3);

  double test_u[Nsamp], test_fu[Nsamp];
  double du = 1.0 / Nsamp;
  for (int i = 0; i < Nsamp; ++i) {
    test_u[i] = (i+1) * du;
    test_fu[i] = test_u[i] * test_u[i] * HOCT_kernel_eval_3D( test_u[i], 1.0);
    // cout << test_u[i] << "\t" << test_fu[i] << "\t" << HOCT_kernel_eval_3D( test_u[i], 1.0) << endl;
  }

  double invNorm = trapIntegral(1, Nsamp, test_u, test_fu);

  invNorm *= 4 * pi;

  normfac = 1.0 / invNorm;
  for (int i = 0; i < Nsamp; ++i) {
    test_u[i] = (i+1) * du;
    test_fu[i] = test_u[i] * test_u[i] * HOCT_kernel_eval_3D( test_u[i], 1.0);
    // cout << test_u[i] << "\t" << HOCT_kernel_eval_3D( test_u[i], 1.0) << endl;
  }
  // cout <<  "\tnormfac: " << normfac <<  "\tinvNorm: " << invNorm << "\talp: " << alp <<  "\tbet: " << bet <<  "\tnn: " << nn <<  "\tAfac: " << Afac <<  "\tBfac: " << Bfac <<  "\tNsamp: " << Nsamp <<  "\tPfac: " << Pfac <<  "\tQfac: " << Qfac <<  "\tkap: " << kap << endl;

  return;
}

//! Utility function which numerically evaluates the trapezoidal rule integration on an array
/*!
  \param indexA index of the left-hand array entry for the integration
  \param indexB index of the right-hand array entry for the integration
  \param Xs array of x values where the evaluated function is f(x)
  \param FXs array of function values where the evaluated function is f(x)
  \return the outcome of the integral calculation
*/
double trapIntegral(int indexA, int indexB, double *Xs, double *FXs) { // trapezium method for integration
  if(indexA < 1) {
    cout << "WARNING: Correcting trapIntegral lower bound" << endl;
    indexA = 1;
  }
  // int n = indexB-indexA;
  double intval = 0;
  for(int i=indexA; i<indexB; i++)
    intval += (Xs[i]-Xs[i-1])*(FXs[i] - (FXs[i]-FXs[i-1])/2.0);

  return intval;
}

//! Function to numerically evaluate and initialise the normalisations of the 2D higher order OCT kernel
void HOCT_kernel_init_2D() {
  HOCT_kernel_init_3D();
  using HOCTvar::Nsamp;
  using HOCTvar::W_tab;
  using HOCTvar::dW_tab;

  try {
    W_tab.Resize(Nsamp);
  } catch (bad_alloc xa) {
    cerr << "ERROR: Could not allocate memory for tabulated 2D HOCT kernel" << endl;
    throw;
  }

  try {
    dW_tab.Resize(Nsamp);
  } catch (bad_alloc xa) {
    cerr << "ERROR: Could not allocate memory for tabulated 2D HOCT gradient kernel" << endl;
    throw;
  }

  double R[Nsamp];
  double Z[Nsamp];
  for (int i = 0; i < Nsamp; ++i) {
    R[i] = i / double(Nsamp-1);
    Z[i] = R[i];
  }

  // double W[Nsamp];
  for (int i = 0; i < Nsamp; ++i) {
    double Ws[Nsamp];
    for (int j = 0; j < Nsamp; ++j) {
      double r = sqrt(R[i]*R[i] + Z[j]*Z[j]);
      Ws[j] = HOCT_kernel_eval_3D(r,1.0);
      // cout << R[i] << "\t" << Z[i] << "\t" << Ws[j] << endl;
    }
    W_tab[i] = 2 * trapIntegral(1, Nsamp, Z, Ws); // fac 2 for both sides of kernel about z=0

    for (int j = 0; j < Nsamp; ++j) {
      double r = sqrt(R[i]*R[i] + Z[j]*Z[j]);
      Ws[j] = HOCT_dw_kernel_eval_3D(r,1.0);
    }
    dW_tab[i] = 2 * trapIntegral(1, Nsamp, Z, Ws); // fac 2 for both sides of kernel about z=0
  }

  return;
}

//! Function returning the value of the 2D higher order OCT kernel at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double HOCT_kernel_eval_2D(double r, double h) {
  using HOCTvar::Nsamp;
  using HOCTvar::W_tab;

  double u = r / h;
  double hinv2 = 1./h/h;
  if((u > 1) || (u < 0))
    return 0;
  else {
    int low_bin = int(floor( u * (Nsamp-1) ));
    int up_bin = low_bin + 1;
    low_bin = (low_bin>=0) ? low_bin : 0;
    up_bin = (up_bin<Nsamp) ? up_bin : Nsamp-1;

    return linterp(u, low_bin / double(Nsamp-1), up_bin / double(Nsamp-1), W_tab[low_bin], W_tab[up_bin]) * hinv2;
  }
}

//! Function returning the value of the 2D higher order OCT kernel gradient at a given position
/*!
  \param r the radius at which to evaluate the kernel
  \param h the kernel smoothing length
  \return kernel value
*/
double HOCT_dw_kernel_eval_2D(double r, double h) {
  using HOCTvar::Nsamp;
  using HOCTvar::dW_tab;

  double u = r / h;
  double hinv2 = 1./h/h;
  if((u > 1) || (u < 0))
    return 0;
  else {
    int low_bin = int(floor( u * (Nsamp-1) ));
    int up_bin = low_bin + 1;
    low_bin = (low_bin>=0) ? low_bin : 0;
    up_bin = (up_bin<Nsamp) ? up_bin : Nsamp-1;

    return linterp(u, low_bin / double(Nsamp-1), up_bin / double(Nsamp-1), dW_tab[low_bin], dW_tab[up_bin]) * hinv2;
  }
}

//! Utility function which numerically evaluates the linear interpolation between supplied values
/*!
  \param x position at which the function is approximated, where function is f(x)
  \param lowx lower coordinate value 
  \param upx upper coordinate value
  \param lowfx lower function value, f(lowx)
  \param upfx upper function value, f(upx)
  \return the outcome of the interpolation calculation
*/
double linterp(double x, double lowx, double upx, double lowfx, double upfx) {
  double fx = ( (upx - x)/(upx - lowx) ) * lowfx + ( (x - lowx)/(upx - lowx) ) * upfx;
  return fx;
}


