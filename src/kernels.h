
#ifndef KERNEL_HEADER
#define KERNEL_HEADER

/**
    \file kernels.h
    A header file containing static parameters and function pointers for use in
    the kernels.cpp file which defines all available SPH smoothing kernels
*/

template <typename T>
class DynamicArray;

double (*kernel_eval)(double r, double h);      //!< function pointer to SPH kernel in use
double (*dw_kernel_eval)(double r, double h);   //!< function pointer to SPH gradient kernel in use

//! Namespace containing static variables relating to the Higher OCT kernel
namespace HOCTvar {

static const double alp = 0.75;                 //!< kernel shape coefficient
static const double bet = 0.5;                  //!< kernel shape coefficient
static const int nn = 6;                        //!< kernel shape coefficient

static const double Afac = (1  -  bet*bet) / (pow(alp, nn  -  3) * (alp*alp  -  bet*bet));  //!< kernel BC normalisations
static const double Bfac =  -  (1 + Afac * pow(alp, nn  -  1)) / pow(bet, nn  -  1);        //!< kernel BC normalisations

static const int Nsamp = 500;                   //!< number of tabulated kernel samples

static double Pfac;                             //!< kernel shape coefficient
static double Qfac;                             //!< kernel shape coefficient
static double kap;                              //!< kernel shape coefficient
static double normfac;                          //!< normalisation constant

static DynamicArray<double> W_tab;              //!< tabulated kernel weight values
static DynamicArray<double> dW_tab;             //!< tabulated kernel weight gradient values

}

//! Namespace containing static variables relating to the Wendland kernels
namespace WCvar {
static double k_w;                              //!< kernel normalisation constant
static double k_dw;                             //!< kernel gradient normalisation constant
}

//! Namespace containing static variables relating to the Cubic Spline kernel
namespace CSvar {
static double k_w;                              //!< normalisation constant
}

#endif
