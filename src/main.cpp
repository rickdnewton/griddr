#include <iostream>
#include <sstream>
#include <stdexcept>
#include <fstream>///////////////////diag

#include <cmath>

#include "globals.h"
#include "dynamic_array.h"
#include "three_d_grid.h"
#include "particle_data.h"
#include "proto.h"
#include "default_error.h"
#include "arginput.h"

using namespace std;

bool compType(ParticleData a, ParticleData b) {
  return (a.type < b.type);
}

#ifndef PERIODIC
inline double dX(double X1, double X0) {
  return X1 - X0;
}
#else
inline double dX(double X1, double X0) {
  double dx = X1 - X0;
  return (dx < head::BoxSize/2.) ? ((dx < -head::BoxSize/2.) ? dx+head::BoxSize : dx) : dx-head::BoxSize;
}
#endif

void echo_params(void);

void scale_coords(void);//<--
void tilt_view(double Theta);//<--


int main( int argc, char *argv[] ) {
  try {
    cout << "\n==============================================================\n"
         "                   N-body Griddr Ver. 0.7\n"
         "==============================================================\n" << endl;

    InitialiseArguments();
    // param_file_location = NULL;


    int run_status = param_cmd_parse(argc-1, &argv[1]);
    if (!run_status) {
      if (!param_file_location.empty()) {
        strip_string_quotes(param_file_location);
        ParameterFileRead(param_file_location.c_str());
      }
      
      init();

      G2_read(input_file_location.c_str(), head::Npart, 0);

      // check BCs
    #ifndef PERIODIC
      if (head::BoxSize>0) {
        cerr << "\n* WARNING: Is the periodic flag set correctly?"
             "\n* \t\tperiodic: 1\tBoxSize: " << head::BoxSize << endl;
      }
    #else
      if(head::BoxSize==0) {
        cerr << "\nERROR: Periodic BC's specified but BoxSize=" << head::BoxSize << " in file header" << endl;
        return 1;
      }
    #endif

      if((x_r0 != 0) || (y_r0 != 0) || (z_r0 != 0) ) {
        translate_sys(-x_r0, -y_r0, -z_r0);
        double HalfBox = head::BoxSize / 2.0;
        for(int i=0; i<N; i++) {
          P[i].pos[0] += HalfBox;
          P[i].pos[1] += HalfBox;
          P[i].pos[2] += HalfBox;
        }
      }
      if((theta_r != 0) || (phi_r != 0)  || (x_r != 0)  || (y_r != 0)  || (z_r != 0) )
        rotate_sys(theta_r, phi_r, x_r, y_r, z_r);

#ifdef SCITECH_TILT
      tilt_view(60);//<--------
#endif

      grid_construct();

      echo_params();

#ifdef PERSPECTIVE
      scale_coords(); /// camera perspective
#endif

      bin_parts();
      // cleanup();/////
      prop_assignment();

      data_write(output_file_location.c_str());



      // Cleanup data
      cleanup();

    } else {
      return 1;
    }

  } catch(FileOpenError err) {

    cerr << err.what() << endl;
    return 1;

  } catch(...) {
    throw;
  }

  cout << endl;
  return 0;
}

void echo_params(void) {
  cout << "\n     -----------------------------------------------------" << endl;

  if((x_r0 != 0) || (y_r0 != 0) || (z_r0 != 0) )
    cout << "Applying data re-centring about [" << x_r0 << ", " << y_r0 << ", " << z_r0 << "]" << endl << endl;

  if((theta_r != 0) || (phi_r != 0) )
    cout << "Rotating object, theta = " << theta_r << ", phi = " << phi_r << endl << endl;

  cout << "Creating grid with dimensions " << NxDim << "x" << NyDim;
#ifdef THREE_DIMS
  cout << "x" << NzDim;
#endif
  cout << endl;

  cout << endl;

  cout << "Grid limits (";
  cout << "set via mode " << LimMode;
  if(LimMode == 0)
    cout << "; enclosing particle type " << grid_inc_type;
  cout << "): " << endl;

  cout << "\tx_min: " << xLim_g[0] << "\t" << "\tx_max: " << xLim_g[1] << endl;
  cout << "\ty_min: " << yLim_g[0] << "\t" << "\ty_max: " << yLim_g[1] << endl;
  cout << "\tz_min: " << zLim_g[0] << "\t" << "\tz_max: " << zLim_g[1] << endl;

  cout << endl;

  cout << "Simulation boundaries: ";
#ifndef PERIODIC
  cout << "non-periodic" << endl;
#else
  cout << "periodic" << endl;
#endif

  cout << endl;

  cout << "Kernel in use: " << sph_kernel_type << endl;

  cout << "\n     -----------------------------------------------------" << endl;
  return;
}

void scale_coords(void) {
  double z0 = xLen_g * 0.8666; //sqrt(3)/2 opening angle of 60 degrees // 1.207 opening angle 45 degrees
  double HalfBox = head::BoxSize / 2.0;


  int rho_idx[] = {0, 0, 0, 0, 0, 0};
  for (int type = 0; type < 6; ++type) {
    if(N_dsets_type[type] <= 0)
      continue;
    stringstream ss;
    ss.str(" ");
    ss << "/PartType" << type << "/Density";
    while((rho_idx[type] < tot_N_dsets) && (props[rho_idx[type]].label.find(ss.str()) == std::string::npos))
      rho_idx[type]++;

    if(rho_idx[type] >= tot_N_dsets) {
      cerr << "ERROR: Expected density data not found at " << ss.str() << endl;
      throw DefaultError("Missing data");
    }
  }

  // ofstream out("test_pos_out.csv");
#ifndef FISHEYE
  for (int type = 0, l_index = 0; type < 6; ++type) {
    int u_index = l_index + head::Npart[type];
    if((head::Npart[type] <= 0) || (N_dsets_type[type] <= 0)){
      l_index = u_index;
      continue;
    }
    for (int i = l_index; i < u_index; i++) {
      double scale = z0 / (z0 + (P[i].pos[2]-zLim_g[0]));
      P[i].pos[0] = (P[i].pos[0] - HalfBox) * scale + HalfBox;
      P[i].pos[1] = (P[i].pos[1] - HalfBox) * scale + HalfBox;
      P[i].h *= scale;
      P[i].extra[props[rho_idx[type]].map] /= pow(scale,2);///////
      // P[i].extra[props[rho_idx[type]].map-1] = scale;///////
      // out << P[i].pos[0] << ", " << P[i].pos[1] << ", " << P[i].pos[2] << ", " << P[i].h << ", " << P[i].extra[props[rho_idx[type]].map] << ", " << scale << endl;/////
    }
    l_index = u_index;
  }
#else
  for (int type = 0, l_index = 0; type < 6; ++type) {
    int u_index = l_index + head::Npart[type];
    if((head::Npart[type] <= 0) || (N_dsets_type[type] <= 0)){
      l_index = u_index;
      continue;
    }

    for (int i = l_index; i < u_index; i++) {

      P[i].pos[2] += zLen_g / 2;//<--------

      double R = sqrt(pow(P[i].pos[0] - HalfBox, 2) + pow(P[i].pos[1] - HalfBox, 2));
      double theta = atan2(R, P[i].pos[2] - HalfBox);
      double scale = 2*theta / (pi / 2.) / R;

      P[i].pos[0] = (P[i].pos[0] - HalfBox) * scale + HalfBox;
      P[i].pos[1] = (P[i].pos[1] - HalfBox) * scale + HalfBox;
      P[i].h *= scale;
      P[i].extra[props[rho_idx[type]].map] /= pow(scale,2);///////
      // P[i].extra[props[rho_idx[type]].map-1] = scale;///////
      // out << P[i].pos[0] << ", " << P[i].pos[1] << ", " << P[i].pos[2] << ", " << P[i].h << ", " << P[i].extra[props[rho_idx[type]].map] << ", " << scale << endl;/////
    }
    l_index = u_index;
  }
#endif
  // out.close();/////
  return;
}

void cleanup(void) {
  // for (int i = 0; i < N; ++i)
  //     delete[] P[i].extra;
  // delete[] P;
  // // delete[] input_file_location;
  // // delete[] output_file_location;
  // delete[] props;
  //
  // for (int i = 0; i < NzDim; ++i) {
  //     for (int j = 0; j < NyDim; ++j) {
  //         for (int k = 0; k < NxDim; ++k) {
  //             delete[] grid[i][j][k].properties;
  //             delete[] grid[i][j][k].N;
  //         }
  //         delete[] grid[i][j];
  //     }
  //     delete[] grid[i];
  // }
  // delete[] grid;

  // clean_args();
}

void tilt_view(double Theta) {
  stringstream ss;
  double newX, newY, newZ;
  // Theta = 0;

#ifndef FISHEYE
  double z0 = xLen_g * 0.8666;
#else
  double z0 = 0;//-zLen_g/2.;
#endif
  double HalfBox = head::BoxSize / 2.0;

   for(int i=0; i<N; i++) {
    P[i].pos[0] -= x_g0; //-= HalfBox;
    P[i].pos[1] -= y_g0; //-= HalfBox;
    P[i].pos[2] -= z_g0; //-= HalfBox;
  }
  translate_sys(0, 0, 0 - (-(zLen_g/2. + z0)));

  // cout << P[0].pos[0] << ",\t" << P[0].pos[1] << ",\t" << P[0].pos[2] << endl;
  // translate_sys(0, 0, -(-(z0 + zLen_g)));
  // cout << P[0].pos[0] << ",\t" << P[0].pos[1] << ",\t" << P[0].pos[2] << endl;
  // translate_sys(0, 0, -(z0 + zLen_g));
  // cout << P[0].pos[0] << ",\t" << P[0].pos[1] << ",\t" << P[0].pos[2] << endl;

  for(int j = 0, l_index = 0; j < 6; ++j) {
    int u_index = l_index + head::Npart[j];

    if(N_dsets_type[j] <= 0) {
      l_index = u_index;
      continue;
    }

    int vx_ind, vy_ind, vz_ind, idx = 0;
    if(use_vels == 1) {
      ss.str(" ");
      ss << "/PartType" << j << "/Velocities_0";
      while((idx < tot_N_dsets) && (props[idx].label.find(ss.str()) == std::string::npos))
        idx++;
      if(idx < tot_N_dsets) {
        vx_ind = props[idx].map;
        vy_ind = vx_ind+1;
        vz_ind = vx_ind+2;
      } else {
        cerr << "ERROR: Expected velocity data not found at " << ss.str() << endl;
        throw DefaultError("Missing data");
      }
      ss.str("");
      ss.clear();
    }

    for(int i = l_index; i < u_index; ++i) {
      double fac = pi / 180;
      // Positions
      newY = (P[i].pos[1] - 0) * cos(Theta*fac) - (P[i].pos[2] - 0) * sin(Theta*fac);
      newZ = (P[i].pos[1] - 0) * sin(Theta*fac) + (P[i].pos[2] - 0) * cos(Theta*fac);
      P[i].pos[1] = (newY + 0);
      P[i].pos[2] = (newZ + 0);

      // Velocities
      if(use_vels == 1) {
        newY = P[i].extra[vy_ind] * cos(Theta*fac) - P[i].extra[vz_ind] * sin(Theta*fac);
        newZ = P[i].extra[vy_ind] * sin(Theta*fac) + P[i].extra[vz_ind] * cos(Theta*fac);
        P[i].extra[vy_ind] = newY;
        P[i].extra[vz_ind] = newZ;
      }
    }
    l_index += head::Npart[j];
  }
  translate_sys(0, 0, 0 - (zLen_g/2. + z0));
  for(int i=0; i<N; i++) {
    P[i].pos[0] += x_g0;//+= HalfBox;
    P[i].pos[1] += y_g0;//+= HalfBox;
    P[i].pos[2] += z_g0;//+= HalfBox;
  }
}
