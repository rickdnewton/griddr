#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>

#include <cstdio>
#include <cassert>

#ifdef HAVE_HDF5
#include <hdf5.h>
#endif

#include "globals.h"
#include "dynamic_array.h"
#include "particle_data.h"
#include "proto.h"
#include "default_error.h"

using namespace std;

/**
    \file G2_read.cpp
    An implementation cpp file containing functions for reading Gadget2 binary and
    HDF5 filetypes into a flexible and dynamically allocated object array.
*/

#ifdef HAVE_HDF5
//! Function to act as custom simple error handler
/*!
  \return HDF5 error status object
*/
herr_t hdf5_error_handler(void *unused) {
  throw Hdf5Error("An HDF5 error was detected");
}

//! Function to probe a HDF5 file and count the properties associated with each particle type
/*!
  \param file target file
  \param N_dsets_type address of array in which to record the number of datasets for each particle type
*/
void query_file_datasets(hid_t file, int *N_dsets_type);

//! Function which prints the label of any discovered data structure and counts any properties which may be smoothed
/*!
  \param loc_id datasest location within HDF5 file
  \param name character array in which discovered dataset label is stored
  \param N_dsets address of integer counter which is incremented when a new property is discovered
  \return HDF5 error status object
*/
herr_t interrogate_object(hid_t loc_id, const char *name, void *N_dsets);

//! Function to control looping over desired data to be read from HDF5 file
/*!
  \param file target file
  \param dset_it address of array in which to record the number of datasets read for each particle type
*/
void read_file_datasets(hid_t file, int *dset_it);

//! Function to perform actual read of data from HDF5 file
/*!
  \param loc_id datasest location within HDF5 file
  \param name character array in which discovered dataset label is stored
  \param dset_it address of integer counter which is incremented when a property is read - extra 7th entry holds current particle type
  \return HDF5 error status object
*/
herr_t read_object(hid_t loc_id, const char *name, void *dset_it);


hid_t file; //!< HDF5 file object
#endif

DynamicArray<ParticleData> P;
DynamicArray<ParticleProperty> props;

int max_N_dsets;
int tot_N_dsets;
int N_dsets_type[6];

double head::BoxSize;


void G2_read(const char *fName, int *npart, int mode) {

  unsigned int dum = 0;
  double Time_t = 0;
  double redshift = 0;
  int flag_sfr = 0;
  int flag_feedback = 0;
  int npartTotal[6];
  double Massarr[6];
  int flag_cooling = 0;
  int num_files = 1;
  head::BoxSize = 0;
  double Omega0 = 0;
  double OmegaLambda = 0;
  double HubbleParam = 0;
  int FlagAge = 0;
  int FlagMetals = 0;
  int NallHW[] = {0, 0, 0, 0, 0, 0};
  int flag_entr_ics = 0;
  // double dt = 0;

  stringstream ss;
  int NpRead = 0;

  cout << "Reading input file: " << input_file_location << endl;

  if ((input_format == 0) || (input_format == 1)) {
    FILE * pFile;

    unsigned int bsize; //variable to hold blocksize

    pFile = fopen ( fName , "rb" );

    if(!pFile) {
      throw FileOpenError("Unable to open input file");
    }

    bsize = 256;
    fread (&bsize, sizeof(unsigned int), 1, pFile);
    fread (npart, sizeof(unsigned int), 6, pFile); ///<---?! &!?
    fread (&Massarr, sizeof(double), 6, pFile);
    fread (&Time_t, sizeof(double), 1, pFile);
    fread (&redshift, sizeof(double), 1, pFile);
    fread (&flag_sfr, sizeof(int), 1, pFile);
    fread (&flag_feedback, sizeof(int), 1, pFile);
    fread (&npartTotal, sizeof(int), 6, pFile);
    fread (&flag_cooling, sizeof(int), 1, pFile);
    fread (&num_files, sizeof(int), 1, pFile);
    fread (&head::BoxSize, sizeof(double), 1, pFile);
    fread (&Omega0, sizeof(double), 1, pFile);
    fread (&OmegaLambda, sizeof(double), 1, pFile);
    fread (&HubbleParam, sizeof(double), 1, pFile);
    fread (&FlagAge, sizeof(int), 1, pFile);
    fread (&FlagMetals, sizeof(int), 1, pFile);
    fread (&NallHW, sizeof(int), 6, pFile);
    fread (&flag_entr_ics, sizeof(int), 1, pFile);
    for(int i=0; i<15; i++) fread (&dum, sizeof(int), 1, pFile); // redundant header space
    fread (&bsize, sizeof(unsigned int), 1, pFile);

    // int NpRead = 0;
    for(int i=0; i<6; i++)
      NpRead += npart[i];

    // allocate memory for particles
    try {
      P.Resize(NpRead);
    } catch (bad_alloc xa) {
      cout << "Could not allocate memory for required particles." << endl;
      fclose(pFile);
      throw;
    }
    cout << "Allocated " << sizeof(ParticleData) * NpRead/1024./1024. << " MB for particles" << endl;

    N += NpRead;

    /////////////////////////////////////////////
    // calc number max number of 'additional' properties
    max_N_dsets = 0;
    tot_N_dsets = 0;

    int N_gas_dsets = 2; // rho u
    if((input_format == 1) && (flag_cooling == 1))
      N_gas_dsets += 2;
    if (use_vels)
      N_gas_dsets += 3; // v_0,1,2
    int N_dm_dsets = 0;  // assuming no density data saved
    N_dsets_type[0] = N_gas_dsets;
    N_dsets_type[1] = N_dm_dsets;   // left separate in case of BHs
    for (int i = 2; i < 6; ++i)
      N_dsets_type[i] = N_dm_dsets;

    for (int i = 0; i < 6; ++i) {
      if(npart[i] == 0)
        N_dsets_type[i] = 0;
    }

    // tally the total and max number of additional datasets needed
    for (int i = 0; i < 6; ++i) {
      tot_N_dsets += N_dsets_type[i];
      max_N_dsets = (N_dsets_type[i]>max_N_dsets) ? N_dsets_type[i] : max_N_dsets;
    }

    cout << "Additional datasets required on particles: " << max_N_dsets << endl << endl;
    cout << "Allocating memory" << endl;

    for (int i = 0; i < N; ++i) {
      try {
        P[i].extra.Resize(max_N_dsets);
      } catch (bad_alloc xa) {
        cerr << "ERROR: Could not allocate memory for additional particle properties at particle: " << i << endl;
        fclose(pFile);
        throw;
      }
    }
    // allocate memory for additional properties
    props.Resize(tot_N_dsets);
    for (int i = 0; i < tot_N_dsets; ++i) {
      props[i].label = "NULL";
      props[i].active = false;
      props[i].map = -1;
      props[i].type = -1;
    }
    /////////////////////////////////////////////

    int idx = 0;
    int dset_it_int[] = {0,0,0,0,0,0};

    DynamicArray<int> i_buff;
    try {
      i_buff.Resize(NpRead);
    } catch (bad_alloc xa) {
      cerr << "ERROR: Could not allocate memory for i_buff read-in array" << endl;
      fclose(pFile);
      throw;
    }

    DynamicArray<float> f3_buff;
    try {
      f3_buff.Resize(NpRead*3);
    } catch (bad_alloc xa) {
      cerr << "ERROR: Could not allocate memory for f3_buff read-in array" << endl;
      fclose(pFile);
      throw;
    }

    // read main file contents

    fread (&bsize, sizeof(unsigned int), 1, pFile);
    cout << "Reading particle positions";
    cout.flush();
    fread (&f3_buff[0], sizeof(float), NpRead*3, pFile);
    for(int i=0; i<NpRead; i++) {
      for(int j=0; j<3; j++)
        P[i].pos[j] = f3_buff[3*i + j];
    }
    cout << "...........complete!" << endl;
    fread (&bsize, sizeof(unsigned int), 1, pFile);

    fread (&bsize, sizeof(unsigned int), 1, pFile);
    cout << "Reading particle velocities";
    cout.flush();
    for(int ptype=0, l_index=0,u_index=0; ptype<6; ++ptype) {
      if(npart[ptype] == 0)
        continue;

      u_index += npart[ptype];

      if((N_dsets_type[ptype] > 0) && (use_vels == 1)) {
        int inds[3];
        stringstream ss_label;
        ss_label.str(" ");
        for (int i = 0; i < 3; ++i) {
          ss_label << "/PartType" << ptype << "/Velocities_" << i;
          props[idx].label = ss_label.str();
          ss_label.str("");
          ss_label.clear();
          props[idx].type = ptype;
          props[idx].map = dset_it_int[ptype];
          idx++;
          inds[i] = dset_it_int[ptype]++;
        }

        fread (&f3_buff[0], sizeof(float), (u_index-l_index)*3, pFile);
        for(int i=l_index; i<u_index; i++) {
          for (int j = 0; j < 3; ++j)
            P[i].extra[inds[j]] = f3_buff[3*(i-l_index) + j];
        }
      } else {
        fread (&f3_buff[0], sizeof(float), (u_index-l_index)*3, pFile);
      }
      l_index = u_index;
    }
    cout << "...........complete!" << endl;
    fread (&bsize, sizeof(unsigned int), 1, pFile);

    fread (&bsize, sizeof(unsigned int), 1, pFile);
    cout << "Reading particle IDs";
    cout.flush();
    fread (&i_buff[0], sizeof(int), NpRead, pFile);
    for(int i=0; i<NpRead; i++) {
      P[i].id = i_buff[i];
    }
    cout << "...........complete!" << endl;
    fread (&bsize, sizeof(unsigned int), 1, pFile);

    bool read_masses = false;
    for (int j = 0, l_index = 0; j < 6; ++j) {
      int u_index = l_index + npart[j];
      if((npart[j] > 0) && (Massarr[j] == 0)) {
        if(!read_masses) {
          cout << "Reading particle massses";
          cout.flush();
          fread (&bsize, sizeof(unsigned int), 1, pFile);
          read_masses = true;
        }

        fread (&f3_buff[0], sizeof(float), u_index-l_index, pFile);
        for(int i=l_index; i<u_index; i++) {
          P[i].mass = f3_buff[i-l_index];
        }
      }
      l_index += npart[j];
    }
    if(read_masses) {
      cout << "...........complete!" << endl;
      fread (&bsize, sizeof(unsigned int), 1, pFile);
    }

    if(npart[0]!=0) {
      bsize = npart[0] * sizeof(float);
      fread (&bsize, sizeof(unsigned int), 1, pFile);
      cout << "Reading gas particle internal energy";

      int l_index = 0;
      int u_index = npart[0];
      int ptype = 0;

      stringstream ss_label;
      ss_label << "/PartType" << ptype << "/InternalEnergy";
      props[idx].label = ss_label.str();
      ss_label.str("");
      ss_label.clear();
      props[idx].type = ptype;
      props[idx].map = dset_it_int[ptype];
      idx++;
      int ind = dset_it_int[ptype]++;

      fread (&f3_buff[0], sizeof(float), bsize/sizeof(float), pFile);
      for(int i=l_index; i<u_index; i++) {
        P[i].extra[ind] = f3_buff[i-l_index];
      }
      cout << "...........complete!" << endl;
      fread (&bsize, sizeof(unsigned int), 1, pFile);
    }

    if(npart[0]!=0) {
      bsize = npart[0] * sizeof(float);
      fread (&bsize, sizeof(unsigned int), 1, pFile);
      cout << "Reading gas particle densities";

      int l_index = 0;
      int u_index = npart[0];
      int ptype = 0;

      stringstream ss_label;
      ss_label << "/PartType" << ptype << "/Density";
      props[idx].label = ss_label.str();
      ss_label.str("");
      ss_label.clear();
      props[idx].type = ptype;
      props[idx].map = dset_it_int[ptype];
      idx++;
      int ind = dset_it_int[ptype]++;

      fread (&f3_buff[0], sizeof(float), bsize/sizeof(float), pFile);
      for(int i=l_index; i<u_index; i++) {
        P[i].extra[ind] = f3_buff[i-l_index];
      }
      cout << "...........complete!" << endl;
      fread (&bsize, sizeof(unsigned int), 1, pFile);
    }

    if((input_format == 1) && (flag_cooling == 1)) {
      if(npart[0]!=0) {
        bsize = npart[0] * sizeof(float);
        fread (&bsize, sizeof(unsigned int), 1, pFile);
        cout << "Reading gas particle electron abunances";

        int l_index = 0;
        int u_index = npart[0];
        int ptype = 0;

        stringstream ss_label;
        ss_label << "/PartType" << ptype << "/ElectronAbundance";
        props[idx].label = ss_label.str();
        ss_label.str("");
        ss_label.clear();
        props[idx].type = ptype;
        props[idx].map = dset_it_int[ptype];
        idx++;
        int ind = dset_it_int[ptype]++;

        fread (&f3_buff[0], sizeof(float), bsize/sizeof(float), pFile);
        for(int i=l_index; i<u_index; i++) {
          P[i].extra[ind] = f3_buff[i-l_index];
        }
        cout << "...........complete!" << endl;
        fread (&bsize, sizeof(unsigned int), 1, pFile);


        fread (&bsize, sizeof(unsigned int), 1, pFile);
        cout << "Reading gas particle neutral hydrogen abundance";

        ss_label << "/PartType" << ptype << "/NeutralHydrogenAbundance";
        props[idx].label = ss_label.str();
        ss_label.str("");
        ss_label.clear();
        props[idx].type = ptype;
        props[idx].map = dset_it_int[ptype];
        idx++;
        ind = dset_it_int[ptype]++;
        fread (&f3_buff[0], sizeof(float), bsize/sizeof(float), pFile);
        for(int i=l_index; i<u_index; i++) {
          P[i].extra[ind] = f3_buff[i-l_index];
        }
        cout << "...........complete!" << endl;
        fread (&bsize, sizeof(unsigned int), 1, pFile);
      }
    }

    if(npart[0]!=0) {
      bsize = npart[0] * sizeof(float);
      fread (&bsize, sizeof(unsigned int), 1, pFile);
      cout << "Reading gas particle smoothing lengths";
      fread (&f3_buff[0], sizeof(float), bsize/sizeof(float), pFile);
      for(int i=0; i<npart[0]; i++) {
        P[i].h = f3_buff[i];
      }
      cout << "...........complete!" << endl;
      fread (&bsize, sizeof(unsigned int), 1, pFile);
    }


    cout << "Closing file" << endl;
    fclose(pFile);

    for (int i = 0, index = 0; i < 6; i++) {
      for (int j = 0; j < npart[i]; j++) {
        P[index].type = i;
        if(Massarr[i] != 0)
          P[index].mass = Massarr[i];
        index++;
      }
    }


  } else if (input_format == 9) {
#ifdef HAVE_HDF5
    hid_t h_grp, grps[6];
    hid_t dataspace, attribute, dataset;
    hsize_t dims[2];
    herr_t status;

    herr_t (*old_func)(void*);
    void *old_client_data;
    H5Eget_auto(&old_func, &old_client_data);

    H5Eset_auto(NULL, NULL);

    file = H5Fopen(fName, H5F_ACC_RDONLY, H5P_DEFAULT);

    if(file < 0) {
      throw FileOpenError("Unable to open input file");
    }
    H5Eset_auto(old_func, old_client_data);

    h_grp = H5Gopen(file, "/Header");

    // read header
    attribute = H5Aopen_name(h_grp, "NumPart_ThisFile");
    H5Aread(attribute, H5T_NATIVE_UINT, npart);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "MassTable");
    H5Aread(attribute, H5T_NATIVE_DOUBLE, Massarr);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Time");
    H5Aread(attribute, H5T_NATIVE_DOUBLE, &Time_t);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Redshift");
    H5Aread(attribute, H5T_NATIVE_DOUBLE, &redshift);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Flag_Sfr");
    H5Aread(attribute, H5T_NATIVE_INT, &flag_sfr);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Flag_Feedback");
    H5Aread(attribute, H5T_NATIVE_INT, &flag_feedback);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "NumPart_Total");
    H5Aread(attribute, H5T_NATIVE_UINT, npartTotal);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Flag_Cooling");
    H5Aread(attribute, H5T_NATIVE_INT, &flag_cooling);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "NumFilesPerSnapshot");
    H5Aread(attribute, H5T_NATIVE_INT, &num_files);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "BoxSize");
    H5Aread(attribute, H5T_NATIVE_DOUBLE, &head::BoxSize);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Omega0");
    H5Aread(attribute, H5T_NATIVE_DOUBLE, &Omega0);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "OmegaLambda");
    H5Aread(attribute, H5T_NATIVE_DOUBLE, &OmegaLambda);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "HubbleParam");
    H5Aread(attribute, H5T_NATIVE_DOUBLE, &HubbleParam);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Flag_StellarAge");
    H5Aread(attribute, H5T_NATIVE_INT, &FlagAge);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Flag_Metals");
    H5Aread(attribute, H5T_NATIVE_INT, &FlagMetals);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "NumPart_Total_HighWord");
    H5Aread(attribute, H5T_NATIVE_UINT, &NallHW);
    H5Aclose(attribute);

    attribute = H5Aopen_name(h_grp, "Flag_Entropy_ICs");
    H5Aread(attribute, H5T_NATIVE_INT, &flag_entr_ics);
    H5Aclose(attribute);



    for(int i=0; i<6; i++)
      NpRead += npart[i];

    // allocate memory for particles
    try {
      P.Resize(NpRead);
    } catch (bad_alloc xa) {
      cout << "Could not allocate memory for required particles." << endl;
      throw;
    }
    cout << "Allocated " << sizeof(ParticleData) * NpRead/1024./1024. << " MB for particles." << endl;

    N += NpRead;
    cout << endl;


    // query number max number of 'additional' properties
    max_N_dsets = 0;
    tot_N_dsets = 0;
    for (int i = 0; i < 6; ++i)
      N_dsets_type[i] = 0;
    query_file_datasets(file, N_dsets_type);

    // make sure there's SmoothingLength data (otherwise we can't smooth it)
    for (int i = 0; i < 6; ++i) {
      int rho_idx = 0;
      stringstream ss;
      ss.str(" ");
      ss << "/PartType" << i << "/SmoothingLength";
      H5Eset_auto(NULL, NULL);
      dataset = H5Dopen(file, ss.str().c_str());
      H5Eset_auto(old_func, old_client_data);
      if(dataset < 0) {
        if(i == 0)
          cout << endl;
        cerr << "WARNING:  smoothing lengths not found at " << ss.str() << ", skipping" << endl;
        N_dsets_type[i] = 0;

        H5Eclear();
      } else
        status = H5Dclose(dataset);
    }

    // tally the total and max number of additional datasets needed
    for (int i = 0; i < 6; ++i) {
      tot_N_dsets += N_dsets_type[i];
      max_N_dsets = (N_dsets_type[i]>max_N_dsets) ? N_dsets_type[i] : max_N_dsets;
    }

    cout << "\nAdditional datasets required on particles: "
         << max_N_dsets << endl << endl;
    cout << "Allocating memory" << endl;

    for (int i = 0; i < N; ++i) {
      try {
        P[i].extra.Resize(max_N_dsets);
      } catch (bad_alloc xa) {
        cerr << "ERROR: Could not allocate memory for additional"
             "particle properties at particle: " << i << endl;
        throw;
      }
    }

    // allocate memory for additional properties
    props.Resize(tot_N_dsets);
    for (int i = 0; i < tot_N_dsets; ++i) {
      props[i].label = "NULL";
      props[i].active = false;
      props[i].map = -1;
      props[i].type = -1;
    }


    // read main file contents
    H5Eset_auto(old_func, old_client_data);

    int dset_it[7];
    for (int i = 0; i < 6; ++i)
      dset_it[i] = 0;

    read_file_datasets(file, dset_it);

    for (int i = 0, index = 0; i < 6; i++) {
      for (int j = 0; j < npart[i]; j++) {
        P[index].type = i;
        if(Massarr[i] != 0)
          P[index].mass = Massarr[i];
        index++;
      }
    }


    cout << "Closing file" << endl;

    status = H5Gclose(h_grp);
    status = H5Fclose(file);

#else
    // cerr << "ERROR: Attempting to read from HDF5 file when libraries are not defined" << endl;
    throw FileAccessError("Attempting to read from HDF5 file when libraries are not defined");
#endif
  } else {

    throw FileAccessError("ERROR: Invalid file format specification");

  }


  return;
}


#ifdef HAVE_HDF5
void query_file_datasets(hid_t file, int *N_dsets_type) {
#ifndef NDEBUG
  assert(input_format == 9);
#endif

  stringstream ss;
  ss.str(" ");
  for (int i = 0; i < 6; i++) {
    if(head::Npart[i] > 0) {
      ss << "/PartType" << i;
      cout << ss.str() << " contains: " << endl;
      H5Giterate(file, ss.str().c_str(), NULL, interrogate_object, &N_dsets_type[i]);
      ss.str("");
      ss.clear();

    }
  }
}


herr_t interrogate_object(hid_t loc_id, const char *name, void *N_dsets) {
  H5G_stat_t statbuf;
  string Sname;
  int *pInt = static_cast<int*>(N_dsets);

  H5Gget_objinfo(loc_id, name, false, &statbuf);

  switch (statbuf.type) {
  case H5G_GROUP:
    cout << "\tA group with name " << name << endl;
    break;
  case H5G_DATASET:
    cout << "\tA dataset with name " << name << endl;
    Sname = name;
    if((Sname.find("Coordinates") != string::npos)      ||
        (Sname.find("ParticleIDs") != string::npos)     ||
        (Sname.find("SmoothingLength") != string::npos) ||
        (Sname.find("Masses") != string::npos)           ) {
      (*pInt) += 0;
    } else if (Sname.find("Velocities") != string::npos) {
      if(use_vels == 1)
        (*pInt) += 3;
    } else {
      (*pInt) += 1;
    }
    break;
  case H5G_TYPE:
    cout << "\tA named datatype with name " << name << endl;
    break;
  default:
    printf(" Unable to identify an object ");
  }
  return 0;
}

void read_file_datasets(hid_t file, int *dset_it) {
#ifndef NDEBUG
  assert(input_format == 9);
#endif

  stringstream ss;
  ss.str(" ");
  for (int i = 0; i < 6; i++) {
    if(N_dsets_type[i] > 0) {
      dset_it[6] = i; // set last (extra) element to current type
      ss << "/PartType" << i;
      cout << "Reading data from " << ss.str() << ": " << endl;
      H5Giterate(file, ss.str().c_str(), NULL, read_object, dset_it);
      ss.str("");
      ss.clear();

    }
  }
}

herr_t read_object(hid_t loc_id, const char *name, void *dset_it) {
  herr_t status;
  H5G_stat_t statbuf;
  stringstream ss;
  ss.str(" ");
  stringstream ss_label;
  ss_label.str(" ");
  int *dset_it_int = static_cast<int*>(dset_it);
  int ptype = dset_it_int[6];
  string Sname = name;

  int u_index, l_index = 0;
  for (int i = 0; i < ptype; ++i)
    l_index += head::Npart[i];
  u_index = l_index + head::Npart[ptype];

  int idx = 0;
  while(props[idx].type != -1)
    idx++;

  H5Gget_objinfo(loc_id, name, false, &statbuf);

#ifndef NDEBUG
  assert(statbuf.type == H5G_DATASET);
#endif

  ss << "/PartType" << ptype << "/" << name;
  hid_t dataset = H5Dopen(file, ss.str().c_str());
  hid_t datatype = H5Dget_type(dataset);

  int Ndim = H5Sget_simple_extent_ndims(H5Dget_space(dataset));
  int Npoints = H5Sget_simple_extent_npoints(H5Dget_space(dataset));
  int lenydim = Npoints / head::Npart[ptype];

#ifndef NDEBUG
  assert((Ndim <= 2) || ((lenydim == 1) && (lenydim == 3)));
#endif


  cout << "\tReading " << name;
  if(H5Tequal(datatype, H5T_NATIVE_UINT) == 1) {

    DynamicArray<int> i_buff;
    try {
      i_buff.Resize(Npoints);
    } catch (bad_alloc xa) {
      cerr << "ERROR: Could not allocate memory for i_buff read-in array, type: " << ptype << endl;
      throw;
    }

    status = H5Dread(dataset, H5T_NATIVE_UINT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &i_buff[0]);

    if(Sname.find("ParticleIDs") != string::npos) {

      for (int p = l_index; p < u_index; ++p)
        P[p].id = i_buff[p-l_index];

    } else {

      for (int j = 0; j < lenydim; ++j) {
        props[idx].type = ptype;
        props[idx].map = dset_it_int[ptype];

        ss_label << ss.str();
        if (lenydim > 1)
          ss_label << "_" << j;
        props[idx].label = ss_label.str();
        ss_label.str("");
        ss_label.clear();

        for (int p = l_index; p < u_index; ++p)
          P[p].extra[dset_it_int[ptype]] = i_buff[lenydim*(p-l_index)+j];

        dset_it_int[ptype]++;
        idx++;
      }
    }


  } else if(H5Tequal(datatype, H5T_NATIVE_FLOAT) == 1) {

    DynamicArray<float> f_buff;
    try {
      f_buff.Resize(Npoints);
    } catch (bad_alloc xa) {
      cerr << "ERROR: Could not allocate memory for f_buff read-in array, type: " << ptype << endl;
      throw;
    }

    status = H5Dread(dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &f_buff[0]);

    if(Sname.find("Coordinates") != string::npos) {

      for (int p = l_index; p < u_index; ++p) {
        for (int j = 0; j < 3; ++j)
          P[p].pos[j] = f_buff[3*(p-l_index)+j];
      }

    } else if(Sname.find("SmoothingLength") != string::npos) {

      for (int p = l_index; p < u_index; ++p)
        P[p].h = f_buff[p-l_index];

    } else if(Sname.find("Masses") != string::npos) {

      for (int p = l_index; p < u_index; ++p)
        P[p].mass = f_buff[p-l_index];

    } else if((Sname.find("Velocities") != string::npos) && (use_vels != 1)) {
      //do nothing, skipping
    } else {

      for (int j = 0; j < lenydim; ++j) {
        props[idx].type = ptype;
        props[idx].map = dset_it_int[ptype];

        ss_label << ss.str();

        if (lenydim > 1)
          ss_label << "_" << j;
        props[idx].label = ss_label.str();
        ss_label.str("");
        ss_label.clear();

        for (int p = l_index; p < u_index; ++p)
          P[p].extra[dset_it_int[ptype]] = f_buff[lenydim*(p-l_index)+j];

        dset_it_int[ptype]++;
        idx++;
      }

    }


  } else {
    cout << "\nERROR: Encountered unexpected type whilst readng " << ss.str() << endl;
  }

  status = H5Dclose(dataset);
  ss.str("");
  ss.clear();
  cout << "...........complete!" << endl;

  return 0;
}
#endif
