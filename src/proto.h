#ifndef PROTO_HEADER
#define PROTO_HEADER

#include <string> // string class

void init(void);
void strip_string_quotes(std::string& s);
void strip_string_quotes(char* s);

void InitialiseArguments(void);
void ParameterFileRead(const char *fName);

void G2_read(const char *fName, int *npart, int mode);

void translate_sys(double X, double Y, double Z);
void rotate_sys(double Theta, double Phi, double x_r, double y_r, double z_r);

void kernel_init(int form);
extern double (*kernel_eval)(double r, double h);
extern double (*dw_kernel_eval)(double r, double h);

inline double kernel_h3(double& h);
inline double kernel_norm(double& h3);

double CS_kernel_eval(double r, double h);
double CS_dw_kernel_eval(double r, double h);
double WC2_kernel_eval(double r, double h);
double WC2_dw_kernel_eval(double r, double h);
double WC4_kernel_eval(double r, double h);
double WC4_dw_kernel_eval(double r, double h);
double WC6_kernel_eval(double r, double h);
double WC6_dw_kernel_eval(double r, double h);
double HOCT_kernel_eval_3D(double r, double h);
double HOCT_dw_kernel_eval_3D(double r, double h);
double HOCT_kernel_eval_2D(double r, double h);
double HOCT_dw_kernel_eval_2D(double r, double h);
void HOCT_kernel_init_3D(void);
void HOCT_kernel_init_2D(void);

double trapIntegral(int indexA, int indexB, double *Xs, double *FXs);
double linterp(double x, double lowx, double upx, double lowfx, double upfx);

inline double dX(double X0, double X1);
inline int dbin(int dbin, int NDim);

void grid_construct(void);
void bin_parts(void);

void prop_assignment(void);

void data_write(const char *fname);

void cleanup(void);

#endif
