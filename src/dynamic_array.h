#ifndef DYNAMIC_ARRAY_HEADER
#define DYNAMIC_ARRAY_HEADER

/**
    \file dynamic_array.h
    A header file containing the DynamicArray class definition.
*/

/*!
  The class DynamicArray template class allows for the creation of a vector-like
  array of arbitrary member object type which may be dynamically resized.
  Standard vector class is not used to ensure minimal resizing occurs.
*/
template <typename T>
class DynamicArray {
 private:
  T* array_;    //!<  pointer to internal storage array
  int length_;  //!<  length of the array (number of member objects)

  //! Function to swap the address in memory of two DynamicArray objects' internal storage arrays
  /*!
    \param target DynamicArray object with which to swap internal storage arrays
  */
  void SwapMemoryLocation(DynamicArray& target) {
    T* temp_ptr = target.array_;
    target.array_ = array_;
    array_ = temp_ptr;

    int temp_length_ = target.length_;
    target.length_ = length_;
    length_ = temp_length_;
  }
 public:
  //! Default constructor, creates empty object
  DynamicArray() {
    length_ = 0;
    array_ = NULL;
  }

  //! Parametrised constructor
  /*!
    \param size desired number of member objects
  */
  DynamicArray(int size) {
    length_ = size;
    try {
      array_ = new T[length_];
    } catch (std::bad_alloc xa) {
      throw;
    }
  }

  //! Copy constructor
  /*!
    \param target DynamicArray object to copy
  */
  DynamicArray(const DynamicArray& target) {
    length_ = target.Size();
    try {
      array_ = new T[length_];
    } catch (std::bad_alloc xa) {
      throw;
    }
    for(int i = 0; i < target.Size(); ++i)
      array_[i] = target[i];
  }

  //! Resizes existing DynamicArray object by creating new object and swapping storage array addresses
  /*!
    \param new_length desired length for new array
  */
  void Resize(int new_length) {
    DynamicArray holder(new_length);
    SwapMemoryLocation(holder);

    // Copy over as much existing data as we have room for
    if(holder.Size() <= length_) {
      for(int i = 0; i < holder.Size(); ++i)
        array_[i] = holder[i];
    } else {
      for(int i = 0; i < length_; ++i)
        array_[i] = holder[i];
    }
  }

  //! Access function
  /*!
    \return length of the array (number of member objects)
  */
  int Size(void) const {
    return length_;
  }

  //! Overloaded access operator to give traditional array-like interface
  /*!
    \param i target index
    \return reference to argument pointer at target index
  */
  T &operator[] (int i) {
// #ifndef NDEBUG
//     assert((i >= 0) && (i < length_))
// #endif
    return array_[i];
  }

  //! Default destructor, frees memory of internal storage array if any has been allocated
  ~DynamicArray() {
    if(!array_)
      delete[] array_;
  }

};


#endif
