#ifndef PARTICLE_DATA_HEADER
#define PARTICLE_DATA_HEADER

/**
    \file particle_data.h
    A header file containing the code objects for holding particle data and data on
    the available smoothable properties and mappings
*/

#include <string> // string class

// forward declaration of template class DynamicArray
template <typename T>
class DynamicArray;

/*!
  Container for all information pertaining to an individual particle
*/
struct ParticleData {
  double pos[3];		             //!<  particle position
  double mass;		               //!<  particle mass
  int id;		                     //!<  particle identifier
  int type;		                   //!<  particle type
  int bin[3];                    //!<  particle host bin in the x, y and z directions
  double h;                      //!<  particle SPH smoothing length

  DynamicArray<double> extra;    //!<  array holding all additional particle properties for use in smoothing calc

  bool active;                   //!<  particle active in current smoothing calculation?

};

/*!
  Container for information on particle properties which may be smoothed on to the grid
*/
struct ParticleProperty {
  std::string label;             //!<  property label
  bool active;                   //!<  property in use for current smoothing calculation?
  int map;                       //!<  property mapping on to index within ParticleData.extra array
  int type;                      //!<  particle type which property is associated with
};

#endif
