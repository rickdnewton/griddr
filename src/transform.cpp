#include <fstream>
#include <iostream>
#include <cmath>
#include <stdexcept>
// #include <algorithm>
#include <string>
#include <sstream>

#include "dynamic_array.h"
#include "particle_data.h"
#include "three_d_grid.h"
#include "globals.h"
#include "proto.h"
#include "default_error.h"

using namespace std;

inline double dX(double X1, double X0) {
  double dx = X1 - X0;
  return (dx < head::BoxSize/2.) ? ((dx < -head::BoxSize/2.) ? dx+head::BoxSize : dx) : dx-head::BoxSize;
}

void translate_sys(double X, double Y, double Z) {
#ifndef PERIODIC
  for(int i=0; i<N; i++) {
    P[i].pos[0] += X;
    P[i].pos[1] += Y;
    P[i].pos[2] += Z;
  }
#else
  double HalfBox = head::BoxSize / 2.0;
  for(int i=0; i<N; i++) {
    P[i].pos[0] = dX(P[i].pos[0] + X, 0);//+HalfBox;
    P[i].pos[1] = dX(P[i].pos[1] + Y, 0);//+HalfBox;
    P[i].pos[2] = dX(P[i].pos[2] + Z, 0);//+HalfBox;
  }
#endif

}

void rotate_sys(double Theta, double Phi, double x_r, double y_r, double z_r) {
  stringstream ss;
  double newX, newY, newZ;
  double HalfBox = head::BoxSize / 2.0;

  for(int j = 0, l_index = 0; j < 6; ++j) {

    int vx_ind, vy_ind, vz_ind, idx = 0;
    if(use_vels == 1) {
      if(N_dsets_type[j] <= 0)
        continue;
      ss.str(" ");
      ss << "/PartType" << j << "/Velocities_0";
      while((idx < tot_N_dsets) && (props[idx].label.find(ss.str()) == std::string::npos))
        idx++;
      if(idx < tot_N_dsets) {
        vx_ind = props[idx].map;
        vy_ind = vx_ind+1;
        vz_ind = vx_ind+2;
      } else {
        cerr << "ERROR: Expected velocity data not found at " << ss.str() << endl;
        throw DefaultError("Missing data");
      }
      ss.str("");
      ss.clear();
    }
    int u_index = l_index + head::Npart[j];

    double fac = pi / 180;

    // Positions

    if(x_r != 0) {
      for(int i = l_index; i < u_index; ++i) {
        newY = (P[i].pos[2] - HalfBox) * sin(x_r*fac) + (P[i].pos[1] - HalfBox) * cos(x_r*fac);
        newZ = (P[i].pos[2] - HalfBox) * cos(x_r*fac) - (P[i].pos[1] - HalfBox) * sin(x_r*fac);
        P[i].pos[1] = (newY + HalfBox);
        P[i].pos[2] = (newZ + HalfBox);
      }
    }

    if(y_r != 0) {
      for(int i = l_index; i < u_index; ++i) {
        newX = (P[i].pos[2] - HalfBox) * sin(y_r*fac) + (P[i].pos[0] - HalfBox) * cos(y_r*fac);
        newZ = (P[i].pos[2] - HalfBox) * cos(y_r*fac) - (P[i].pos[0] - HalfBox) * sin(y_r*fac);
        P[i].pos[0] = (newX + HalfBox);
        P[i].pos[2] = (newZ + HalfBox);
      }
    }

    if(z_r != 0) {
      for(int i = l_index; i < u_index; ++i) {
        newX = (P[i].pos[1] - HalfBox) * sin(z_r*fac) + (P[i].pos[0] - HalfBox) * cos(z_r*fac);
        newY = (P[i].pos[1] - HalfBox) * cos(z_r*fac) - (P[i].pos[0] - HalfBox) * sin(z_r*fac);
        P[i].pos[0] = (newX + HalfBox);
        P[i].pos[1] = (newY + HalfBox);
      }
    }

    if(Phi != 0) {
      for(int i = l_index; i < u_index; ++i) {
        newX = (P[i].pos[0] - HalfBox) * cos(Phi*fac) - (P[i].pos[1] - HalfBox) * sin(Phi*fac);
        newY = (P[i].pos[0] - HalfBox) * sin(Phi*fac) + (P[i].pos[1] - HalfBox) * cos(Phi*fac);
        P[i].pos[0] = (newX + HalfBox);
        P[i].pos[1] = (newY + HalfBox);
      }
    }

    if(Theta != 0) {
      for(int i = l_index; i < u_index; ++i) {
        newX = (P[i].pos[0] - HalfBox) * cos(Theta*fac) - (P[i].pos[2] - HalfBox) * sin(Theta*fac);
        newZ = (P[i].pos[0] - HalfBox) * sin(Theta*fac) + (P[i].pos[2] - HalfBox) * cos(Theta*fac);
        P[i].pos[0] = (newX + HalfBox);
        P[i].pos[2] = (newZ + HalfBox);
      }
    }

      // Velocities
    if(use_vels == 1) {
      if(x_r != 0) {
        for(int i = l_index; i < u_index; ++i) {
          newY = (P[i].extra[vz_ind] - HalfBox) * sin(x_r*fac) + (P[i].extra[vy_ind] - HalfBox) * cos(x_r*fac);
          newZ = (P[i].extra[vz_ind] - HalfBox) * cos(x_r*fac) - (P[i].extra[vy_ind] - HalfBox) * sin(x_r*fac);
          P[i].extra[vy_ind] = (newY + HalfBox);
          P[i].extra[vz_ind] = (newZ + HalfBox);
        }
      }

      if(y_r != 0) {
        for(int i = l_index; i < u_index; ++i) {
          newX = (P[i].extra[vz_ind] - HalfBox) * sin(y_r*fac) + (P[i].extra[vx_ind] - HalfBox) * cos(y_r*fac);
          newZ = (P[i].extra[vz_ind] - HalfBox) * cos(y_r*fac) - (P[i].extra[vx_ind] - HalfBox) * sin(y_r*fac);
          P[i].extra[vx_ind] = (newX + HalfBox);
          P[i].extra[vz_ind] = (newZ + HalfBox);
        }
      }

      if(z_r != 0) {
        for(int i = l_index; i < u_index; ++i) {
          newX = (P[i].extra[vy_ind] - HalfBox) * sin(z_r*fac) + (P[i].extra[vx_ind] - HalfBox) * cos(z_r*fac);
          newY = (P[i].extra[vy_ind] - HalfBox) * cos(z_r*fac) - (P[i].extra[vx_ind] - HalfBox) * sin(z_r*fac);
          P[i].extra[vx_ind] = (newX + HalfBox);
          P[i].extra[vy_ind] = (newY + HalfBox);
        }
      }

      if(Phi != 0) {
        for(int i = l_index; i < u_index; ++i) {
          newX = P[i].extra[vx_ind] * cos(Phi*fac) - P[i].extra[vy_ind] * sin(Phi*fac);
          newY = P[i].extra[vx_ind] * sin(Phi*fac) + P[i].extra[vy_ind] * cos(Phi*fac);
          P[i].extra[vx_ind] = newX;
          P[i].extra[vy_ind] = newY;
        }
      }

      if(Theta != 0) {
        for(int i = l_index; i < u_index; ++i) {
          newX = P[i].extra[vx_ind] * cos(Theta*fac) - P[i].extra[vz_ind] * sin(Theta*fac);
          newZ = P[i].extra[vx_ind] * sin(Theta*fac) + P[i].extra[vz_ind] * cos(Theta*fac);
          P[i].extra[vx_ind] = newX;
          P[i].extra[vz_ind] = newZ;
        }
      }
    }
    l_index += head::Npart[j];
  }
}
