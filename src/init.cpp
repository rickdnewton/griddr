#include <fstream>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>

#include "globals.h"
#include "proto.h"
#include "init.h"
using namespace std;

/**
    \file init.cpp
    An implementation cpp file containing functions for initialising the main operation 
    of the code. Current use limited to initialising variables and initialising 
    the SPH kernel.
*/

//! Main initialisation function
void init(void) {

  pi = acos(-1);

  for(int i=0; i<6; i++) {
    head::Npart[i] = 0;
    head::Npart[i] = 0;
  }

  N = 0;

#ifndef THREE_DIMS
  NzDim = 1;
#endif

  strip_string_quotes(input_file_location);
  strip_string_quotes(output_file_location);
  kernel_init(sph_kernel_type);

  return;
}

//! Simple function which searches a string for surrounding quote characters and removes them
/*!
  \param s address of string to operate on
*/
void strip_string_quotes(std::string& s) {
  int it = 0;
  char quotes[] = {'\'', '\"'};
  while (it < 2) {
    if ((s[0] == quotes[it]) && (s[s.length()-1] == quotes[it])) {
      s = s.substr(1,s.size()-2);
      break;
    } else {
      ++it;
    }
  }
  return;
}
