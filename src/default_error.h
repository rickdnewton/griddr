#ifndef DEFAULT_ERROR_HEADER
#define DEFAULT_ERROR_HEADER

/**
    \file default_error.h
    A header file containing the code objects for describing various errors through
    exception handling. All classes take a descriptive message in their constructor
    to be reported to the user.
*/

/*!
  Class used for default runtime error reporting
*/
class DefaultError : public std::runtime_error {
public:
  DefaultError(const char* what_arg) : std::runtime_error(what_arg){}
};

/*!
  Class used for reporting file access errors (other than in opening the file)
*/
class FileAccessError : public DefaultError {
public:
  FileAccessError(const char* what_arg) : DefaultError(what_arg){}
};

/*!
  Class used for reporting errors in opening files
*/
class FileOpenError : public FileAccessError {
public:
  FileOpenError(const char* what_arg) : FileAccessError(what_arg){}
};

#ifdef HAVE_HDF5
/*!
  Class used for reporting any HDf5 errors
*/
class Hdf5Error : public std::runtime_error {
public:
  Hdf5Error(const char* what_arg) : std::runtime_error(what_arg){}
};
#endif

#endif
