#ifndef GLOBALS_HEADER
#define GLOBALS_HEADER

/**
    \file globals.h
    A header file containing all global scope variables
*/

#include <string> // string

// prototyping various objects to allow declaration in header
template <typename T>
class DynamicArray;

struct ParticleData;
struct GridVertex;
struct ParticleProperty;

template <typename T>
class ThreeDGrid;


// global data structures
extern DynamicArray<ParticleData> P;          //!<  main array of particle data
extern ThreeDGrid<GridVertex> grid;           //!<  main 3D array of grid data on to which particle properties are smoothed
extern DynamicArray<ParticleProperty> props;  //!<  array of discovered particles properties available for smoothing

// Universal Constants
extern double pi;                             //!<  pi (3.14159265...)

//! Namespace containing variables read from the Gadget snapshot file header
namespace head {
  extern int Npart[6];                        //!<  number of particles by type
  extern double BoxSize;                      //!<  simulation box size (used for periodic simulations)
}

//
extern int N;                                 //!<  total number of particles

extern int max_N_dsets;                       //!<  highest number of datasets (properties) on any given particle type
extern int tot_N_dsets;                       //!<  total number of datasets (properties)
extern int N_dsets_type[6];                   //!<  number of datasets (properties) by particle type
extern int active_type_map[6];                //!<  if type is active in calc stores index where 0<=index<N_types_active

// File locations
extern std::string input_file_location;       //!<  path to target snapshot file
extern std::string output_file_location;      //!<  path to output file
extern std::string param_file_location;       //!<  path to target parameter file

//
extern int input_format;                      //!<  snapshot input file format
extern int output_format;                     //!<  griddr output file format
extern int sph_kernel_type;                   //!<  SPH kernel in use

extern int use_vels;                          //!<  if use_vels!=1, 3D velocity components are not smoothed on to the grid

extern double x_r;                            //!<  rotation angle (degs) around the x-axis
extern double y_r;                            //!<  rotation angle (degs) around the y-axis
extern double z_r;                            //!<  rotation angle (degs) around the z-axis
extern double theta_r;                        //!<  rotation angle (degs) in the theta direction
extern double phi_r;                          //!<  rotation angle (degs) in the phi direction
extern double x_r0;                           //!<  coordinate translation new centre (rotate about this pos.)
extern double y_r0;                           //!<  coordinate translation new centre (rotate about this pos.)
extern double z_r0;                           //!<  coordinate translation new centre (rotate about this pos.)

extern int NxDim;                             //!<  number of grid cells (pixels) in the x direction
extern int NyDim;                             //!<  number of grid cells (pixels) in the y direction
extern int NzDim;                             //!<  number of grid cells (pixels) in the z direction

extern int LimMode;                           //!<  smoothing grid limit calculation mode
extern int grid_inc_type;                     //!<  particle type to enclose if grid limits auto calculated

extern double xLen_g;                         //!<  length of the smoothing grid in the x direction
extern double yLen_g;                         //!<  length of the smoothing grid in the y direction
extern double zLen_g;                         //!<  length of the smoothing grid in the z direction
extern double x_g0;                           //!<  smoothing grid x-dimension centre
extern double y_g0;                           //!<  smoothing grid y-dimension centre
extern double z_g0;                           //!<  smoothing grid z-dimension centre

extern double dx_grid;                        //!<  smoothing grid x-dimension cell length
extern double dy_grid;                        //!<  smoothing grid y-dimension cell length
extern double dz_grid;                        //!<  smoothing grid z-dimension cell length

extern int BoxSize_bx;                        //!<  simulation x-dimension side length measured in dx_grid
extern int BoxSize_by;                        //!<  simulation y-dimension side length measured in dy_grid
extern int BoxSize_bz;                        //!<  simulation z-dimension side length measured in dz_grid

extern double xLim_g[2];                      //!<  smoothing grid x-dimension [lower, upper] limits
extern double yLim_g[2];                      //!<  smoothing grid y-dimension [lower, upper] limits
extern double zLim_g[2];                      //!<  smoothing grid z-dimension [lower, upper] limits


#endif
