#include <fstream>
#include <iostream>
#include <cmath>
#include <algorithm> //sort
#include <string>
#include <sstream>
#include <stdexcept>
#include <iomanip> //setprecision

#ifdef _OPENMP
#include <omp.h>
#endif

#include "dynamic_array.h"
#include "particle_data.h"
#include "three_d_grid.h"
#include "globals.h"
#include "proto.h"
#include "default_error.h"

using namespace std;

/**
    \file grid.cpp
    An implementation cpp file containing functions for constructing the projection
    grid and for performing the smoothing operation of particle properties on to
    the grid.
*/


ThreeDGrid<GridVertex> grid;  //!<  main 3D array of grid data on to which particle properties are smoothed
int active_type_map[6];       //!<  if type is active in calc stores index where 0<=index<N_types_active
static int N_types_active;    //!<  number of particle types active in calculations

int BoxSize_bx;               //!<  simulation x-dimension side length measured in dx_grid
int BoxSize_by;               //!<  simulation y-dimension side length measured in dy_grid
int BoxSize_bz;               //!<  simulation z-dimension side length measured in dz_grid

// used for FastPlot
ThreeDGrid<double> dr2Tab;    //!<  tabulated r^2 values
static int hMax_Nx;           //!<  number of cells spanned in x dir. by largest smoothing length
static int hMax_Ny;           //!<  number of cells spanned in y dir. by largest smoothing length
static int hMax_Nz;           //!<  number of cells spanned in z dir. by largest smoothing length
static double hMax2;          //!<  largest particle smoothing length ^2
//

//! Comparison function for use in sort, arranges particles so active particles are first
/*!
  \param a pair containing particle ID and active state
  \param b pair containing particle ID and active state
  \return boolean for use in sort function
*/
bool compActive(pair<int,bool> a, pair<int,bool> b) {
  return (a.second > b.second);
}

// double (*dX)(double x0, double x1);
// std::function<double(double, double)> dX;

#ifndef PERIODIC

//! Function returns difference in positions in arbitrary dimension, non-periodic
/*!
  \param X0 position for differencing
  \param X1 position for differencing
  \return distance between coordinates
*/
inline double dX(double X0, double X1) {
  return X1 - X0;
}

//! Function which simply returns inputted number of bins/cells between points since non-periodic
/*!
  \param dbin number of bins/cells between points
  \param NDim number of bins/cells in relevant dimension
  \return number of bins between two points
*/
inline int dBin(int dbin, int NDim) {
  return dbin;
}

//! Function which simply returns inputted index since non-periodic
/*!
  \param index number of bins/cells between points
  \param bmin index of (hypothetical) grid cell at siimulation lower limit in relevant dimension
  \param bsize length of bin/cell in relevant direction
  \return bin index
*/
inline int grid_index(int index, int bmin, int bsize) {
  return index;
}

#else

//! Function which computes difference in positions in arbitrary dimension, periodic
/*!
  \param X0 position for differencing
  \param X1 position for differencing
  \return periodically wrapped distance between two coordinates
*/
inline double dX(double X0, double X1) {
  double dx = X1 - X0;
  return (dx < head::BoxSize/2.) ? ((dx < -head::BoxSize/2.) ? dx+head::BoxSize : dx) : dx-head::BoxSize;
}

//! Function which periodically wraps difference in bin/cell index
/*!
  \param dbin number of bins/cells between points
  \param NDim number of bins/cells in relevant dimension
  \return number of bins between two points
*/
inline int dBin(int dbin, int NDim) {
  return (dbin < (NDim-1)/2) ? ((dbin < -(NDim-1)/2) ? dbin+(NDim-1) : dbin) : dbin-(NDim-1);
}

//! Function which returns inputted index since non-periodic
/*!
  \param index number of bins/cells between points
  \param bmin index of (hypothetical) grid cell at siimulation lower limit in relevant dimension
  \param bsize length of bin/cell in relevant direction
  \return wrapped bin index
*/
inline int grid_index(int index, int bmin, int bsize) {
  return (index+bmin >= 0) ? ((index+bmin < bsize) ? index : (index+bmin)-bsize) : bsize+(index+bmin);
}

#endif

#ifndef THREE_DIMS
#ifndef FAST_PLOT

//! Function which calculates the 2D radial distance between a particle and grid point
/*!
  \param p particle under consideration
  \param g grid point/cell under consideration
  \param i 3D grid z index (unused in this function version)
  \param j 3D grid y index (unused in this function version)
  \param k 3D grid x index (unused in this function version)
  \return radial distance
*/
inline double calc_r2(ParticleData* p, GridVertex* g, int i, int j, int k) {
  return pow(dX(g->x_mid, p->pos[0]), 2) + pow(dX(g->y_mid, p->pos[1]), 2);
}

#else

//! Function which returns the tabulated the 2D radial distance between a particle (assumed to lie at the centre of a cell) and grid point
/*!
  \param p particle under consideration
  \param g grid point/cell under consideration
  \param i 3D grid z index
  \param j 3D grid y index
  \param k 3D grid x index (unused in this function version)
  \return radial distance
*/
inline double calc_r2(ParticleData* p, GridVertex* g, int i, int j, int k) {
  int dx = abs(dBin(k - p->bin[0], BoxSize_bx));
  int dy = abs(dBin(j - p->bin[1], BoxSize_by));
  if ((dx>=hMax_Nx) || (dy>=hMax_Ny))
    return hMax2;
  else
    return dr2Tab(0, dy, dx);
}

#endif

#else

#ifndef FAST_PLOT

//! Function which calculates the 3D radial distance between a particle and grid point
/*!
  \param p particle under consideration
  \param g grid point/cell under consideration
  \param i 3D grid z index (unused in this function version)
  \param j 3D grid y index (unused in this function version)
  \param k 3D grid x index (unused in this function version)
  \return radial distance
*/
inline double calc_r2(ParticleData* p, GridVertex* g, int i, int j, int k) {
  return pow(dX(g->x_mid, p->pos[0]), 2) + pow(dX(g->y_mid, p->pos[1]), 2) + pow(dX(g->z_mid, p->pos[2]), 2);
}

#else

//! Function which calculates the 3D radial distance between a particle (assumed to lie at the centre of a cell) and grid point
/*!
  \param p particle under consideration
  \param g grid point/cell under consideration
  \param i 3D grid z index
  \param j 3D grid y index
  \param k 3D grid x index
  \return radial distance
*/
inline double calc_r2(ParticleData* p, GridVertex* g, int i, int j, int k) {
  int dx = abs(dBin(k - p->bin[0], BoxSize_bx));
  int dy = abs(dBin(j - p->bin[1], BoxSize_by));
  int dz = abs(dBin(i - p->bin[2], BoxSize_bz));
  if ((dx>=hMax_Nx) || (dy>=hMax_Ny) || (dz>=hMax_Nz))
    return hMax2;
  else
    return dr2Tab(dz, dy, dx);
}
#endif
#endif

// double (*calc_r2)(double x0, double x1, double y0, double y1, double z0, double z1);
//
// inline double calc_r2_2D(double x0, double x1, double y0, double y1, double z0, double z1) {
//     return pow(dX(x0, x1), 2) + pow(dX(y0, y1), 2);
// }
//
// inline double calc_r2_3D(double x0, double x1, double y0, double y1, double z0, double z1) {
//     return pow(dX(x0, x1), 2) + pow(dX(y0, y1), 2) + pow(dX(z0, z1), 2);
// }


//! Function which constructs the grid onto which particle properties are smoothed
void grid_construct(void) {

  // count number of types in use
  N_types_active = 0;
  for (int i = 0; i < 6; ++i) {
    if(N_dsets_type[i] > 0)
      active_type_map[i] = N_types_active++;
    else
      active_type_map[i] = -1;
  }

  // allocate memory for grid
  if (!grid.Exist()) {
    try {
      grid.Initialise(NzDim, NyDim, NxDim);
    } catch (bad_alloc xa) {
      cerr << "ERROR: Could not allocate memory for grid.\n";
      throw;
    }
    for (int i = 0; i < NzDim; ++i)	{
      for (int j = 0; j < NyDim; ++j) {
        for (int k = 0; k < NxDim; ++k) {
          try {
            grid(i, j, k).properties.Resize(tot_N_dsets);
          } catch (bad_alloc xa) {
            cerr << "ERROR: Could not allocate memory for additional particle properties at gridpoint: " << i << ", " << j << ", " << k << endl;
            throw;
          }
          try {
            grid(i, j, k).N.Resize(N_types_active);
          } catch (bad_alloc xa) {
            cerr << "ERROR: Could not allocate memory for particle weight storage at gridpoint: " << i << ", " << j << ", " << k << endl;
            throw;
          }
        }
      }
    }
  }


  if((LimMode == 0) || LimMode == 1) {

    // define limits of grid around particles
    double xlim[] = {0,0};
    double ylim[] = {0,0};
    double zlim[] = {0,0};

    double offset = 0.0;

#ifndef PERIODIC

    int l_index, u_index;
    if(grid_inc_type == -1) {
      l_index = 0;
      u_index = N;
    } else {
      l_index = 0;
      for (int i = 0; i < grid_inc_type; ++i)
        l_index += head::Npart[i];
      u_index = l_index + head::Npart[grid_inc_type];
    }

    for (int i = l_index; i < u_index; ++i) {
      offset = P[i].h;

      xlim[0] = (P[i].pos[0]-offset<xlim[0]) ? P[i].pos[0]-offset : xlim[0];
      ylim[0] = (P[i].pos[1]-offset<ylim[0]) ? P[i].pos[1]-offset : ylim[0];
      zlim[0] = (P[i].pos[2]-offset<zlim[0]) ? P[i].pos[2]-offset : zlim[0];

      xlim[1] = (P[i].pos[0]+offset>xlim[1]) ? P[i].pos[0]+offset : xlim[1];
      ylim[1] = (P[i].pos[1]+offset>ylim[1]) ? P[i].pos[1]+offset : ylim[1];
      zlim[1] = (P[i].pos[2]+offset>zlim[1]) ? P[i].pos[2]+offset : zlim[1];
    }

#else
    xlim[0] = 0;
    ylim[0] = 0;
    zlim[0] = 0;
    xlim[1] = head::BoxSize;
    ylim[1] = head::BoxSize;
    zlim[1] = head::BoxSize;
#endif

    double dx = xlim[1] - xlim[0];
    double dy = ylim[1] - ylim[0];
    double dz = zlim[1] - zlim[0];

    double margin = 1e-10;          //!< margin factor increases side lengths so particles are included despite rounding errors
    xlim[0] -= margin*dx;
    ylim[0] -= margin*dy;
    zlim[0] -= margin*dz;

    xlim[0] += margin*dx;
    ylim[0] += margin*dy;
    zlim[0] += margin*dz;


    if(LimMode == 1) {

      // copy bounding limits if not set from parameter file
      if((xLim_g[0] == -1) && (xLim_g[1] == -1)) {
        xLim_g[0] = xlim[0];
        xLim_g[1] = xlim[1];
      }
      if((yLim_g[0] == -1) && (yLim_g[1] == -1)) {
        yLim_g[0] = ylim[0];
        yLim_g[1] = ylim[1];
      }
      if((zLim_g[0] == -1) && (zLim_g[1] == -1)) {
        zLim_g[0] = zlim[0];
        zLim_g[1] = zlim[1];
      }

    } else {

      // copy bounding limits since in auto mode
      xLim_g[0] = xlim[0];
      xLim_g[1] = xlim[1];

      yLim_g[0] = ylim[0];
      yLim_g[1] = ylim[1];

      zLim_g[0] = zlim[0];
      zLim_g[1] = zlim[1];

    }

  } else if (LimMode == 2) {

    // match pixel resolution from defined axis if only one length is set for x,y
    if((xLen_g == -1) && (yLen_g == -1)) {
      throw DefaultError("Neither axis length defined, must define x or y length");
    } else if(xLen_g == -1) {
      xLen_g = NxDim * yLen_g / NyDim;
    } else if (yLen_g == -1) {
      yLen_g = NyDim * xLen_g / NxDim;
    }

    // set grid minima and maxima from the centre position and side lengths
    xLim_g[0] = x_g0 - xLen_g / 2.0;
    yLim_g[0] = y_g0 - yLen_g / 2.0;
    zLim_g[0] = z_g0 - zLen_g / 2.0;

    xLim_g[1] = xLim_g[0] + xLen_g;
    yLim_g[1] = yLim_g[0] + yLen_g;
    zLim_g[1] = zLim_g[0] + zLen_g;

  }

  // initialise grid
  dx_grid = (xLim_g[1] - xLim_g[0]) / NxDim;
  dy_grid = (yLim_g[1] - yLim_g[0]) / NyDim;
  dz_grid = (zLim_g[1] - zLim_g[0]) / NzDim;


  // save head::BoxSize in grid units
#ifdef PERIODIC
  BoxSize_bx = ceil(head::BoxSize / dx_grid);
  BoxSize_by = ceil(head::BoxSize / dy_grid);
  BoxSize_bz = ceil(head::BoxSize / dz_grid);
#endif

  // store grid positions and initialise properties and weights to 0
  for (int i = 0; i < NzDim; ++i) {
    double z = zLim_g[0] + dz_grid * (i + 0.5);
    for (int j = 0; j < NyDim; ++j) {
      double y = yLim_g[0] + dy_grid * (j + 0.5);
      for (int k = 0; k < NxDim; ++k) {
        double x = xLim_g[0] + dx_grid * (k + 0.5);

        grid(i, j, k).x_mid = x;
        grid(i, j, k).y_mid = y;
        grid(i, j, k).z_mid = z;

        for (int d = 0; d < tot_N_dsets; ++d)
          grid(i, j, k).properties[d] = 0;

        for (int d = 0; d < N_types_active; ++d)
          grid(i, j, k).N[d] = 0;

      }
    }
  }

  return;
}


//! Function calculates which bin/cell particles lie in by assuming cells are evenly spaced
void bin_parts(void) {
  for (int i = 0; i < N; i++) {
    P[i].bin[0] = floor( (P[i].pos[0] - xLim_g[0]) / dx_grid );
    P[i].bin[1] = floor( (P[i].pos[1] - yLim_g[0]) / dy_grid );
    P[i].bin[2] = floor( (P[i].pos[2] - zLim_g[0]) / dz_grid );
  }
  return;
}

//! Function calculates which bin/cell particles lie in by assuming cells are evenly spaced
int check_active_parts(DynamicArray< pair<int,bool> >& order) {
  int Nactive = 0;
  for (int type = 0, l_index = 0; type < 6; ++type) {
    int u_index = l_index + head::Npart[type];
    if((head::Npart[type] <= 0) || (N_dsets_type[type] <= 0)) {
      for (int p = l_index; p < u_index; ++p) {
        order[p].first = p;
        order[p].second = false;
      }
    } else {
      for (int p = l_index; p < u_index; ++p) {
        order[p].first = p;
        order[p].second = true;

        if(P[p].h == 0)
          order[p].second = false;

        // check if part out of range of rendered grid

#ifndef PERIODIC

        if( ((P[p].pos[0] - xLim_g[0]) < -P[p].h) ||
            ((P[p].pos[0] - xLim_g[1]) >  P[p].h) ||
            ((P[p].pos[1] - yLim_g[0]) < -P[p].h) ||
            ((P[p].pos[1] - yLim_g[1]) >  P[p].h) ||
            ((P[p].pos[2] - zLim_g[0]) < -P[p].h) ||
            ((P[p].pos[2] - zLim_g[1]) >  P[p].h)  )
          order[p].second = false;

#else

        double low_deltaX[3], up_deltaX[3];
        low_deltaX[0] = dX(xLim_g[0], P[p].pos[0]);
        low_deltaX[1] = dX(yLim_g[0], P[p].pos[1]);
        low_deltaX[2] = dX(zLim_g[0], P[p].pos[2]);
        up_deltaX[0] = dX(xLim_g[1], P[p].pos[0]);
        up_deltaX[1] = dX(yLim_g[1], P[p].pos[1]);
        up_deltaX[2] = dX(zLim_g[1], P[p].pos[2]);

        if(low_deltaX[0] == up_deltaX[0]) {

        } else if(fabs(low_deltaX[0]) < fabs(up_deltaX[0])) {
          if(low_deltaX[0] < -P[p].h) {
            order[p].second = false;
          }
        } else {
          if(up_deltaX[0] > P[p].h) {
            order[p].second = false;
          }
        }
        if(order[p].second) {
          if(low_deltaX[1] == up_deltaX[1]) {

          } else if(fabs(low_deltaX[1]) < fabs(up_deltaX[1])) {
            if(low_deltaX[1] < -P[p].h) {
              order[p].second = false;
            }
          } else {
            if(up_deltaX[1] > P[p].h) {
              order[p].second = false;
            }
          }
        }
        if(order[p].second) {
          if(low_deltaX[2] == up_deltaX[2]) {

          } else if(fabs(low_deltaX[2]) < fabs(up_deltaX[2])) {
            if(low_deltaX[2] < -P[p].h) {
              order[p].second = false;
            }
          } else {
            if(up_deltaX[2] > P[p].h) {
              order[p].second = false;
            }
          }
        }

#endif

#ifdef PERSPECTIVE
        ///////
        if (P[p].pos[2] - zLim_g[0] < 0) // clip any particles near to the camera
          order[p].second = false;
        if ((P[p].h > xLen_g ) || (P[p].h > yLen_g )) // for when this fails and we still have huge particles flashing on screen, ignore any particles bigger than the image plane
          order[p].second = false;
        ///////
#endif

        if(order[p].second)
          ++Nactive;
      }
    }
    l_index += head::Npart[type];
  }
  return Nactive;
}


void eval_lims(int* lXBin, int* uXBin, int NDim, int bmin, int bsize) {
  int TlXBin, TuXBin;
  for (int i = *lXBin, I; i < *uXBin; ++i) {
    I = grid_index(i, bmin, bsize);
    if( (I < 0) ||
        (I >= NDim) )
      continue;
    TlXBin = i;
    break;
  }

  for (TuXBin = TlXBin; TuXBin < *uXBin; ++TuXBin) {
    int I = grid_index(TuXBin, bmin, bsize);
    if( (I >= NDim) ||
        (I < 0) )
      break;
  }

  *lXBin = TlXBin;
  *uXBin = TuXBin;

  return;
}

void tabulate_hsml_r(double hMax, int clean) {
  hMax_Nx = ceil( hMax / dx_grid);
  hMax_Ny = ceil( hMax / dy_grid);
  hMax_Nz = 1;
#ifdef THREE_DIMS
  hMax_Nz = ceil( hMax / dz_grid);
#endif

  // allocate memory for radius table
  try {
    dr2Tab.Initialise(hMax_Nz, hMax_Ny, hMax_Nx);
  } catch (bad_alloc xa) {
    cerr << "ERROR: Could not allocate memory for hsml grid.\n";
    throw;
  }

  //tabulate radii
  double dx_grid2 = pow(dx_grid, 2);
  double dy_grid2 = pow(dy_grid, 2);
  double dz_grid2 = pow(dz_grid, 2);
  for (int i = 0; i < hMax_Nz; ++i) {
    for (int j = 0; j < hMax_Ny; ++j) {
      for (int k = 0; k < hMax_Nx; ++k) {
#ifndef THREE_DIMS
        dr2Tab(i, j, k) = pow(k, 2)*dx_grid2 + pow(j, 2)*dy_grid2;
#else
        dr2Tab(i, j, k) = pow(k, 2)*dx_grid2 + pow(j, 2)*dy_grid2 + pow(i, 2)*dz_grid2;
#endif
      }
    }
  }

  return;
}

void prop_assignment(void) {

  int BSize_dbinx = head::BoxSize / dx_grid;
  int BSize_dbiny = head::BoxSize / dy_grid;
  int BSize_dbinz = head::BoxSize / dz_grid;

  int g_bminx = int(floor(xLim_g[0] / dx_grid));
  int g_bminy = int(floor(yLim_g[0] / dy_grid));
  int g_bminz = int(floor(zLim_g[0] / dz_grid));

  DynamicArray< pair<int,bool> > order;       // transform to be centred about 0 & tessellate if necessary
  try {
    order.Resize(N);
  } catch (bad_alloc xa) {
    cerr << "ERROR: Could not allocate memory required for order.\n";
    throw;
  }

  int Nactive = check_active_parts(order);
  sort(&order[0], &order[0]+N, compActive);

  int rho_idx[] = {0, 0, 0, 0, 0, 0};
  for (int type = 0; type < 6; ++type) {
    if(N_dsets_type[type] <= 0)
      continue;
    stringstream ss;
    ss.str(" ");
    ss << "/PartType" << type << "/Density";
    while((rho_idx[type] < tot_N_dsets) && (props[rho_idx[type]].label.find(ss.str()) == std::string::npos))
      rho_idx[type]++;

    if(rho_idx[type] >= tot_N_dsets) {
      cerr << "ERROR: Expected density data not found at " << ss.str() << endl;
      throw DefaultError("Missing data");
    }
  }


  // set up radius determination functions
#ifdef FAST_PLOT
  double hMax = P[order[0].first].h;
  for (int i = 0, p; i < Nactive; ++i)
    hMax = (hMax > P[order[i].first].h) ? hMax : P[order[i].first].h;
  hMax2 = pow(hMax, 2);

  tabulate_hsml_r(hMax, 0);
#endif

  cout << "\nNumber of active particles: " << Nactive << "/" << N << endl;

  cout << "Smoothing particles to grid";
  cout.flush();

  int Nsects = 10;
  int subN = Nactive / Nsects;
  int sects[Nsects+1];
  for(size_t i = 0; i < Nsects; i++) {
    sects[i] = subN * i;
  }
  sects[Nsects] = Nactive;

  for (int s = 0; s < Nsects; ++s) {

    #pragma omp parallel for
    for (int it = sects[s]; it < sects[s+1]; ++it) {
      int p = order[it].first;

      // determine smoothed extent in bins
      int h_Nx = floor( P[p].h / dx_grid);
      int h_Ny = floor( P[p].h / dy_grid);

      int lxBin = P[p].bin[0] - h_Nx;
      int lyBin = P[p].bin[1] - h_Ny;

      int uxBin = P[p].bin[0] + h_Nx + 1;
      int uyBin = P[p].bin[1] + h_Ny + 1;

      eval_lims(&lxBin, &uxBin, NxDim, g_bminx, BSize_dbinx);
      eval_lims(&lyBin, &uyBin, NyDim, g_bminy, BSize_dbiny);

      int lzBin, uzBin;

#ifdef THREE_DIMS
      int h_Nz = floor( P[p].h / dz_grid);
      lzBin = P[p].bin[2] - h_Nz;
      uzBin = P[p].bin[2] + h_Nz + 1;
      eval_lims(&lzBin, &uzBin, NzDim, g_bminz, BSize_dbinz);
#else
      lzBin = 0;
      uzBin = 1;
#endif

      int type, lX_idx = 0;
      for (type = 0; type < P[p].type; ++type)
        lX_idx += N_dsets_type[type];
      int uX_idx = lX_idx + N_dsets_type[type];

      int rhop_index = props[rho_idx[type]].map;
      int N_index = active_type_map[type];
      double P_v = P[p].mass / P[p].extra[rhop_index];

      // loop over affected bins
      for (int i = lzBin; i < uzBin; ++i) {
        int I = grid_index(i, g_bminz, BSize_dbinz);
        for (int j = lyBin, J; j < uyBin; ++j) {
          J = grid_index(j, g_bminy, BSize_dbiny);
          for (int k = lxBin, K; k < uxBin; ++k) {
            K = grid_index(k, g_bminx, BSize_dbinx);

            double r2 = calc_r2(&P[p], &grid(I, J, K), I, J, K);

            if(r2 > pow(P[p].h,2))
              continue;
            double r = sqrt(r2);

            // calculate weight
            double W = P_v * kernel_eval(r, P[p].h);

            #pragma omp atomic
            grid(I, J, K).N[N_index] += W;

            double wX;
            // add weighted & smoothed properties to gridpoints
            for(int X_idx = lX_idx; X_idx < uX_idx; ++X_idx) {
              wX = P[p].extra[props[X_idx].map];
              wX *= W;

              #pragma omp atomic
              grid(I, J, K).properties[X_idx] += wX;
            }

          }
        }
      }
    }
    if(s > 0)
      cout << "\b\b\b\b\b\b\b\b\b\b\b\b";
    cout << "." << s << "0\% complete";
    cout.flush();
  }
  cout << "\b\b\b\b\b\b\b\b\b\b\b\bcomplete!   " << endl;


#ifdef FAST_PLOT
  tabulate_hsml_r(0.0, 1);  // clean r table
#endif

  // cout << "complete!" << endl;
  return;
}


void data_write(const char *fname) {

  ofstream out(fname);
  if (out.is_open()) {
    cout << "\nWriting to ascii file: " << fname << endl;

    out << "(0)x coord, (1)y coord, (2)z coord";

    int c = 3;
    for (int t = 0; t < 6; ++t) {
      if(active_type_map[t] != -1) {
        out << ", (" << c << ")" << "N_" << t;
        ++c;
      }
    }

    if(output_format == 0) {
      for (int d = 0; d < tot_N_dsets; ++d){
        out << ", (" << c << ")" << props[d].label;
        ++c;
      }
    }
    out << endl;

    for (int i = 0 ; i < NzDim; ++i) {
      for (int j = 0 ; j < NyDim; ++j) {
        for (int k = 0 ; k < NxDim; ++k) {
          out << std::setprecision(9) << grid(i, j, k).x_mid << "," << grid(i, j, k).y_mid << "," << grid(i, j, k).z_mid;

          for (int d = 0; d < N_types_active; ++d)
            out << "," << grid(i, j, k).N[d];

          if(output_format == 0) {
            for (int d = 0; d < tot_N_dsets; ++d)
              out << "," << grid(i, j, k).properties[d];
          }
          out << endl;
        }
      }
    }
    out.close();
  } else {
    cerr << "ERROR: Unable to open ascii file" << endl;
  }

  return;
}
