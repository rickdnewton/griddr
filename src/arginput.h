#ifndef ARG_IN_HEADER
#define ARG_IN_HEADER

/**
    \file arginput.h
    A header file containing function prototypes for the corresponding .cpp file.
*/

void InitialiseArguments(void);
void PrintUsage(void);
void ParameterFileRead(const char *param_file_name);
int param_cmd_parse(int argc, char *argv[]);

#endif
