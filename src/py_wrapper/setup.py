from distutils.core import setup, Extension

module1 = Extension('griddr',
                    define_macros = [('HAVE_HDF5',None), ('H5_USE_16_API',None), ('PERIODIC',None)],
                    # include_dirs = ['//anaconda/include'],
                    libraries = ['hdf5','m'],
                    # extra_compile_args = ['-fopenmp'],
                    # extra_link_args = ['-lgomp'],
                    # library_dirs = ['//anaconda/lib'],
                    sources = ['griddrmodule.cpp', 'griddr_py_body.cpp', '../globals.cpp', '../arginput.cpp', '../init.cpp', '../G2_read.cpp', '../transform.cpp', '../grid.cpp', '../kernels.cpp'],
                    # headers = ['../proto.h', '../particle_data.h', '../globals.h', '../init.h']
                    )

setup (name = 'griddr',
       version = '1.0',
       description = 'This is a demo package',
       ext_modules = [module1])
