#include <Python.h>

#include <string>
#include <sstream>
#include <iostream> //////
#include <stdexcept>

#include "../globals.h"
#include "../dynamic_array.h"
#include "../three_d_grid.h"
#include "../particle_data.h"
#include "../proto.h"
#include "../default_error.h"

#include "griddr_py_body.h"

////
extern "C" {

  static PyObject* griddr_wrapper(PyObject* self, PyObject* args) {
    /////// test for returning one double value
    int Narg = 0;
    char** arguments;
    char* argstr;
    std::string argstring;
    ThreeDGrid<GridVertex> grid;
    PyObject* retdict;

    if (!PyArg_ParseTuple(args, "s", &argstr))
      return NULL;

    argstring = argstr;
    while (argstring.size() > 0) {
      int end = argstring.find_first_of(' ');
      end = (end < argstring.size()) ? end : argstring.size();

      if (end < argstring.size())
        argstring = argstring.substr(end+1, argstring.size());
      else
        argstring = "";
      ++Narg;
    }

    arguments = new char*[Narg];

    int it = 0;
    argstring = argstr;
    while (argstring.size() > 0) {
      int end = argstring.find_first_of(' ');
      end = (end < argstring.size()) ? end : argstring.size();

      std::string temp = argstring.substr(0, end);

      arguments[it] = new char[temp.size()+1];
      strcpy(arguments[it], temp.c_str() );

      if (end < argstring.size())
        argstring = argstring.substr(end+1, argstring.size());
      else
        argstring = "";
      ++it;
    }

    try {
      grid = griddr_body(Narg, arguments);
    } catch (DefaultError err) {
      std::cerr << err.what() << std::endl;
      PyObject* dummy;
      return dummy;
    }



    PyObject* tempList;
    retdict = PyDict_New();


    PyDict_SetItemString(retdict, "x", PyList_New(NxDim*NyDim*NzDim));
    tempList = PyDict_GetItemString(retdict, "x");
    for (int i = 0; i < NzDim; ++i) {
      for (int j = 0; j < NyDim; ++j) {
        for (int k = 0; k < NxDim; ++k) {
          int I = i*NyDim*NxDim + j*NxDim + k;
          PyList_SET_ITEM(tempList, I, PyFloat_FromDouble(grid(i, j, k).x_mid));
        }
      }
    }

    PyDict_SetItemString(retdict, "y", PyList_New(NxDim*NyDim*NzDim));
    tempList = PyDict_GetItemString(retdict, "y");
    for (int i = 0; i < NzDim; ++i) {
      for (int j = 0; j < NyDim; ++j) {
        for (int k = 0; k < NxDim; ++k) {
          int I = i*NyDim*NxDim + j*NxDim + k;
          PyList_SET_ITEM(tempList, I, PyFloat_FromDouble(grid(i, j, k).y_mid));
        }
      }
    }

    PyDict_SetItemString(retdict, "z", PyList_New(NxDim*NyDim*NzDim));
    tempList = PyDict_GetItemString(retdict, "z");
    for (int i = 0; i < NzDim; ++i) {
      for (int j = 0; j < NyDim; ++j) {
        for (int k = 0; k < NxDim; ++k) {
          int I = i*NyDim*NxDim + j*NxDim + k;
          PyList_SET_ITEM(tempList, I, PyFloat_FromDouble(grid(i, j, k).z_mid));
        }
      }
    }

    std::stringstream ss;
    ss.str(" ");
    for (int d = 0; d < 6; ++d) {
      if(active_type_map[d] != -1) {
        ss << "N_" << d;
        PyDict_SetItemString(retdict, ss.str().c_str(), PyList_New(NxDim*NyDim*NzDim));
        tempList = PyDict_GetItemString(retdict, ss.str().c_str());
        for (int i = 0; i < NzDim; ++i) {
          for (int j = 0; j < NyDim; ++j) {
            for (int k = 0; k < NxDim; ++k) {
              int I = i*NyDim*NxDim + j*NxDim + k;
              PyList_SET_ITEM(tempList, I, PyFloat_FromDouble(grid(i, j, k).N[d]));
            }
          }
        }
        ss.str("");
        ss.clear();
      }
    }

    for (int d = 0; d < tot_N_dsets; ++d) {
      PyDict_SetItemString(retdict, props[d].label.c_str(), PyList_New(NxDim*NyDim*NzDim));
      tempList = PyDict_GetItemString(retdict, props[d].label.c_str());
      for (int i = 0; i < NzDim; ++i) {
        for (int j = 0; j < NyDim; ++j) {
          for (int k = 0; k < NxDim; ++k) {
            int I = i*NyDim*NxDim + j*NxDim + k;
            PyList_SET_ITEM(tempList, I, PyFloat_FromDouble(grid(i, j, k).properties[d]));
          }
        }
      }
    }


    return retdict;
  }


  static PyMethodDef griddrMethods[] = {
    { "griddr", griddr_wrapper, METH_VARARGS, "executes Griddr" },
    { NULL, NULL, 0, NULL }       /* Sentinel */
  };


  PyMODINIT_FUNC
  initgriddr(void) {
    (void) Py_InitModule("griddr", griddrMethods);
  }

}
