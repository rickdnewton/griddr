#include <iostream>

#include <cmath>
#include <stdexcept>

#include "../globals.h"
#include "../dynamic_array.h"
#include "../three_d_grid.h"
#include "../particle_data.h"
#include "../proto.h"
#include "../default_error.h"
#include "../arginput.h"

#include "griddr_py_body.h"

using namespace std;

bool compType(ParticleData a, ParticleData b) {
  return (a.type < b.type);
}

void echo_params(void);


ThreeDGrid<GridVertex>griddr_body( int argc, char** argv ) {
  try {
    cout << "\n==============================================================\n"
         "                   N-body Griddr Ver. 0.6\n"
         "==============================================================\n" << endl;

    InitialiseArguments();
    // param_file_location = NULL;


    int run_status = param_cmd_parse(argc, &argv[0]);
    if (!run_status) {
      if (!param_file_location.empty()) {
        strip_string_quotes(param_file_location);
        ParameterFileRead(param_file_location.c_str());
      }

      setGlobals();
      init();

      G2_read(input_file_location.c_str(), npart, 0);

      // check BCs
    #ifndef PERIODIC
      if (BoxSize>0) {
        cerr << "\n* WARNING: Is the periodic flag set correctly?"
             "\n* \t\tperiodic: 1\tBoxSize: " << BoxSize << endl;
      }
    #else
      if(BoxSize==0) {
        cerr << "\nERROR: Periodic BC's specified but BoxSize=" << BoxSize << " in file header" << endl;
        throw DefaultError("Invalid boundary conditions");
      }
    #endif

      if((x_r0 != 0) || (y_r0 != 0) || (z_r0 != 0) )
        translate_sys(-x_r0, -y_r0, -z_r0);
      if((theta_r != 0) || (phi_r != 0) )
        rotate_sys(theta_r, phi_r);

      grid_construct();

      echo_params();

      bin_parts();
      // cleanup();/////
      prop_assignment();

      // data_write(output_file_location.c_str());



      // Cleanup data

    }

  } catch(...) {
    throw;
  }

  cout << endl;
  return grid;
}

void echo_params(void) {
  cout << "\n\t\t\t-----------------------------------------\n" << endl;

  if((x_r0 != 0) || (y_r0 != 0) || (z_r0 != 0) )
    cout << "Applying data re-centring about [" << x_r0 << ", " << y_r0 << ", " << z_r0 << "]" << endl << endl;

  if((theta_r != 0) || (phi_r != 0) )
    cout << "Rotating object, theta = " << theta_r << ", phi = " << phi_r << endl << endl;

  cout << "Creating grid with dimensions " << NxDim << "x" << NyDim;
#ifdef THREE_DIMS
  cout << "x" << NzDim;
#endif
  cout << endl;

  cout << endl;

  cout << "Grid Limits (";
  cout << "set via mode " << LimMode;
  if(LimMode == 0)
    cout << "; enclosing particle type " << grid_inc_type;
  cout << "): " << endl;

  cout << "\tx_min: " << xLim_g[0] << "\t" << "\tx_max: " << xLim_g[1] << endl;
  cout << "\ty_min: " << yLim_g[0] << "\t" << "\ty_max: " << yLim_g[1] << endl;
  cout << "\tz_min: " << zLim_g[0] << "\t" << "\tz_max: " << zLim_g[1] << endl;

  cout << endl;

  cout << "Simulation boundaries: ";
#ifndef PERIODIC
  cout << "non-periodic" << endl;
#else
  cout << "periodic" << endl;
#endif

  cout << endl;

  cout << "Kernel in use: " << sph_kernel_type << endl;

  cout << "\n\t\t\t-----------------------------------------" << endl;
  return;
}
