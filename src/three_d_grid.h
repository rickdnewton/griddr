#ifndef THREE_D_GRID_HEADER
#define THREE_D_GRID_HEADER
/**
    \file three_d_grid.h
    A header file containing the code objects for generic template 3D data structures
    and for the grid vertices which will use them
*/

// forward declaration of template class DynamicArray
template <typename T>
class DynamicArray;


/*!
  Container for coordinates and values at grid vertex i.e. cell centre
*/
struct GridVertex {

  double x_mid; //!< x-coordinate of cell centre
  double y_mid; //!< y-coordinate of cell centre
  double z_mid; //!< z-coordinate of cell centre

  DynamicArray<double> N;  //!< array holding weighted 'number' of particles at gridpoint (for each type)
  DynamicArray<double> properties; //!< array holding the smoothed property values at the cell centre

};



/*!
  Template class describing a 3D grid of arbitrary data type, used principally as the grid upon which smoothed properties are calculated
*/
template <typename T>
class ThreeDGrid {

 private:
  T*** data_; //!< root pointer to data holding 3D array
  int num_x_; //!< number of cells in x dimension
  int num_y_; //!< number of cells in y dimension
  int num_z_; //!< number of cells in z dimension

  //! Performs a swap of the pointers to the data arrays within two ThreeDGrid objects
  /*!
    \param target target object with which the swap takes place
  */
  void SwapMemoryLocation(ThreeDGrid& target) {
    T*** temp_ptr = target.data_;
    target.data_ = data_;
    data_ = temp_ptr;

    int temp_num = target.num_x_;
    target.num_x_ = num_x_;
    num_x_ = temp_num;

    temp_num = target.num_y_;
    target.num_y_ = num_y_;
    num_y_ = temp_num;

    temp_num = target.num_z_;
    target.num_z_ = num_z_;
    num_z_ = temp_num;
  }

 public:
  //! Default constructor
  ThreeDGrid() {
    data_ = NULL;
    num_x_ = 0;
    num_y_ = 0;
    num_z_ = 0;
  }

  //! Parametrised constructor
  /*!
    \param num_z_idx desired number of cells in the z direction
    \param num_y_idx desired number of cells in the y direction
    \param num_x_idx desired number of cells in the x direction
  */
  ThreeDGrid(int num_z_dim, int num_y_dim, int num_x_dim) {
    try {
      data_ = new T**[num_z_dim];
    } catch (std::bad_alloc xa) {
      throw;
    }
    for (int i = 0; i < num_z_dim; ++i) {
      try {
        data_[i] = new T*[num_y_dim];
      } catch (std::bad_alloc xa) {
        throw;
      }

      for (int j = 0; j < num_y_dim; ++j) {
        try {
          data_[i][j] = new T[num_x_dim];
        } catch (std::bad_alloc xa) {
          throw;
        }
      }
    }
    num_x_ = num_x_dim;
    num_y_ = num_y_dim;
    num_z_ = num_z_dim;
  }


//! Accesses a 3D grid of arbitrary member data type, three input indices are mapped to the underlying data structure
/*!
  \param z_idx target z index
  \param y_idx target y index
  \param x_idx target x index
  \return address of target variable
*/
  T& operator()(int z_idx, int y_idx, int x_idx) {
// #ifndef NDEBUG
//     assert((z_idx >= 0) && (z_idx < num_z_))
//     assert((y_idx >= 0) && (y_idx < num_y_))
//     assert((x_idx >= 0) && (x_idx < num_x_))
// #endif
    return data_[z_idx][y_idx][x_idx];
  }


  //! Initialises initially empty existing grid structure, allocating the memory for the vertices
  /*!
    \param num_z_idx desired number of cells in the z direction
    \param num_y_idx desired number of cells in the y direction
    \param num_x_idx desired number of cells in the x direction
    \sa SwapMemoryLocation()
  */
  void Initialise(int num_z_dim, int num_y_dim, int num_x_dim) {
// #ifndef NDEBUG
//     assert(!data_);
// #endif
    if(!data_) {
      ThreeDGrid holder(num_z_dim, num_y_dim, num_x_dim);
      SwapMemoryLocation(holder);
    }
  }

  //! Tests if the grid exists by checking if the underlying data stucture has been allocated
  /*!
    \return The test results (boolean)
  */
  bool Exist(void) const {
    return data_!=NULL;
  }
};

#endif
