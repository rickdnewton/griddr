#ifndef INPUT_ARGUMENT_HEADER
#define INPUT_ARGUMENT_HEADER

#include <string> // string
#include <iostream>  // cout for diagnostics

/**
    \file input_argument.h
    A header file containing the code objects for describing the input parameters;
    parameters are templated and may be any type, some special cases exist for string
    and bool types.
*/

/*!
  Base virtual class upon which GeneralInputArgument template and all specialisations
  are built. Common base class exists to allow for use of base class pointers in
  arrays to loop over all arguments
  \sa GeneralInputArgument
*/
class BaseInputArgument {
 public:
  virtual const char* description() const = 0;
  virtual const char* flag() const = 0;
  virtual bool file_argument() const = 0;
  virtual bool terminal_argument() const = 0;

  virtual bool found_in_terminal() const = 0;
  virtual void set_found_in_terminal(bool) = 0;

  virtual void set_target_variable(std::string val_string) = 0;
  virtual void set_target_variable() = 0;
  virtual ~BaseInputArgument() {};
};


/*!
  Class containing all information relating to an individual code parameter;
  whether it may be found on the command line, via the parameter file or both, as
  well as holding pointers to target variables in the code and any default values.
  \sa BaseInputArgument
*/
template <typename T>
class GeneralInputArgument : public BaseInputArgument {
  private:
    const char* description_; //!< description of code argument/variable (displayed on terminal help screen)
    const char* flag_;        //!< string to be searched for on command line as flag
    T* target_variable_;      //!< pointer to the variable where inputted values will be stored

    bool file_argument_;      //!< if True, argument is searched for in parameter file
    bool terminal_argument_;  //!< if True, argument is searched for in terminal input
    bool found_in_terminal_;  //!< if True, flag GeneralInputArgument.flag_ was found in terminal input

  public:

    //! Constructor for defining code argument by parameters
    /*!
      \param desc description of code argument/variable (displayed on terminal help screen)
      \param flag string to be searched for on command line as flag
      \param p pointer to the variable where inputted values will be stored
      \param default_value=NULL default value for the variable if not defined by parameter file or terminal
      \param in_pfile=true True, argument is searched for in parameter file
      \param c_val=true if True, argument is searched for in terminal input
      \sa GeneralInputArgument(GeneralInputArgument const& arg)
    */
    GeneralInputArgument(const char* desc, const char* flag, T* p, T default_value=NULL, bool in_pfile=true, bool c_val=true) {
      description_ = desc;
      flag_ = flag;
      target_variable_ = p;
      *target_variable_ = default_value;

      file_argument_ = in_pfile;
      terminal_argument_ = c_val;
      found_in_terminal_ = false;
    }

    //! Copy constructor
    /*!
      \param arg existing code argument GeneralInputArgument object
      \sa GeneralInputArgument(const char* desc, const char* flag, T* p, T default_value=NULL, bool in_pfile=true, bool c_val=true)
    */
    GeneralInputArgument(GeneralInputArgument const& arg) {
      description_ = arg.description_;
      flag_ = arg.flag_;
      target_variable_ = arg.target_variable_;

      file_argument_ = arg.file_argument_;
      terminal_argument_ = arg.terminal_argument_;
      found_in_terminal_ = arg.found_in_terminal_;
    }

    //! Default destructor
    ~GeneralInputArgument() {}

    //! Access function
    //! \return argument description
    const char* description() const {
      return description_;
    }

    //! Access function
    //! \return argument command line flag
    const char* flag() const {
      return flag_;
    }

    //! Access function
    //! \return if argument is to be searched for in the parameter file
    bool file_argument() const {
      return file_argument_;
    }

    //! Access function
    //! \return if argument is to be searched for on the command line
    bool terminal_argument() const {
      return terminal_argument_;
    }


    //! Access function
    //! \return if the argument/flag was found in the command line input
    bool found_in_terminal() const {
      return found_in_terminal_;
    }

    //! Access function, sets the argument state to store if it was in the command line input
    //! \param found if the argumet/flag was found in the command line input
    void set_found_in_terminal(bool found) {
      found_in_terminal_ = found;
    }

    //! Access function, sets the argument's corresponding variable value; virtual function
    /*!
      \param val_string input string to convert to target type
      \sa set_target_variable()
    */
    void set_target_variable(std::string val_string) {}

    //! Access function, sets the argument's corresponding variable value in case where no inputted value is necessary; virtual function
    /*!
      \sa set_target_variable(std::string val_string)
    */
    void set_target_variable() {}

    //! Access function
    //! \return a pointer to the argument variable in memory
    T* target_variable() const {
      return target_variable_;
    }

};

/*!
  Access function specialisation for `int`, sets the argument's corresponding variable value
  \param val_string input string to convert to `int`
  \sa set_target_variable(std::string val_string)
*/
template <>
void GeneralInputArgument<int>::set_target_variable(std::string val_string) {
  *target_variable_ = atoi(val_string.c_str());
}

/*!
  Access function specialisation for `float`, sets the argument's corresponding variable value
  \param val_string input string to convert to `float`
  \sa set_target_variable(std::string val_string)
*/
template <>
void GeneralInputArgument<float>::set_target_variable(std::string val_string) {
  *target_variable_ = atof(val_string.c_str());
}

/*!
  Access function specialisation for `double`, sets the argument's corresponding variable value
  \param val_string input string to convert to `double`
  \sa set_target_variable(std::string val_string)
*/
template <>
void GeneralInputArgument<double>::set_target_variable(std::string val_string) {
  *target_variable_ = atof(val_string.c_str());
}

// template <>
// void GeneralInputArgument<char*>::set_target_variable(std::string val_string) {
//     *target_variable_ = new char[val_string.size()+1];
//     std::strcpy(*target_variable_, val_string.c_str() );
// }

/*!
  Access function specialisation for `string`, sets the argument's corresponding variable value
  \param val_string input string to save as `string`
  \sa set_target_variable(std::string val_string)
*/
template <>
void GeneralInputArgument<std::string>::set_target_variable(std::string val_string) {
  *target_variable_ = val_string;
}

/*!
  Access function specialisation for `bool`, sets the argument's corresponding variable value
  \param val_string input string to convert to `bool`
  \sa set_target_variable(std::string val_string)
*/
template <>
void GeneralInputArgument<bool>::set_target_variable(std::string val_string) {
  *target_variable_ = atoi(val_string.c_str()) != 0;
}

/*!
  Access function specialisation for bool with no input value, presence of flag indicates `True`
  \param val_string input string to convert to int
  \sa set_target_variable()
*/
template <>
void GeneralInputArgument<bool>::set_target_variable() {
  *target_variable_ = true;
}

// //! Destructor for inputted charachter array, frees memory (currently unused)
// template <>
// GeneralInputArgument<char*>::~GeneralInputArgument() {
//   delete[] *target_variable_;
// }

/*!
  Class for containing and accessing a list of code input arguments of different
  fundamental variable types though the use of base class pointers and the vector class
  \sa BaseInputArgument, GeneralInputArgument
*/
class ArgumentList {
  private:
    std::vector<BaseInputArgument*> list_;  //!< vector of base class pointers to hold argument addresses
    int length_;                            //!< number of members in the argument list
  public:

    //! Default constructor, initialises empty list with length 0
    ArgumentList() {
      list_.clear();
      length_ = 0;
    }
    // void AppendArgument(BaseInputArgument* target_variable_) {
    //     list_.push_back(target_variable_);
    //     return;
    // }

    //! Access function
    /*!
      \return list length (number of member objects)
    */
    const int Length(void) const {
      return length_;
    }

    //! Appends a copy of and input argument object to the list
    /*!
      \param arg the input argument to be appended
    */
    template <typename T>
    void AppendArgument(GeneralInputArgument<T> arg) {
      list_.push_back(new GeneralInputArgument<T>(arg));
      ++length_;
      return;
    }

    //! Overloaded access operator to give array-like interface
    /*!
      \param i target index
      \return reference to argument pointer at target index
    */
    BaseInputArgument* &operator[] (int i) {
      return list_[i];
    }

    //! Access iterator for member vector
    /*!
      \return member vector iterator for begin
    */
    std::vector<BaseInputArgument*>::iterator Begin() {
      return list_.begin();
    }

    //! Access iterator for member vector
    /*!
      \return member vector iterator for end
    */
    std::vector<BaseInputArgument*>::iterator End() {
      return list_.end();
    }

    //! Default destructor
    //! iterates over list deleting member objects
    ~ArgumentList() {
      for (std::vector<BaseInputArgument*>::iterator it = list_.begin(); it != list_.end(); ++it) {
        delete *it;
      }
    }
};


#endif
