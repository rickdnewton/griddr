
##########################################

# OPT += -DTHREE_DIMS
# OPT += -DFAST_PLOT
OPT += -DPERSPECTIVE
# OPT += -DFISHEYE
# OPT += -DSCITECH_TILT

OPT += -DHAVE_HDF5 -DH5_USE_16_API


##########################################

CC = g++             # sets the C++ compiler
HDF5_INCL = -I/opt/local/include
HDF5_LIBS = -L/opt/local/lib -lhdf5

OPENMP_FLAGS =


##########################################

EXEC_ROOT = Griddr

SOURCE = main.cpp arginput.cpp init.cpp G2_read.cpp transform.cpp grid.cpp kernels.cpp

OPTIMFLAGS = -O3

CFLAGS = $(OPT) $(OPTIMFLAGS) $(HDF5_INCL) $(OPENMP_FLAGS)
LFLAGS = $(OPENMP_FLAGS)

LIBS = $(HDF5_LIBS) -lm

PERIODIC_FLAG = PERIODIC
PERIODIC_OBJS_DIR = p_objs
NONPERIODIC_OBJS_DIR = np_objs
EXEC_P_SUFFIX = -P
EXEC_NP_SUFFIX = -NP
EXEC_DIR = ../bin

##########################################

OBJS = $(SOURCE:.cpp=.o)
PERIODIC_OBJS = $(addprefix $(PERIODIC_OBJS_DIR)/, $(OBJS))
NONPERIODIC_OBJS = $(addprefix $(NONPERIODIC_OBJS_DIR)/, $(OBJS))

EXEC_P = $(addprefix $(EXEC_ROOT), $(EXEC_P_SUFFIX))
EXEC_NP = $(addprefix $(EXEC_ROOT), $(EXEC_NP_SUFFIX))

EXECS = $(EXEC_P) $(EXEC_NP)


##########################################

all : $(EXECS)

$(EXEC_P) : $(PERIODIC_OBJS)
	@mkdir -p $(EXEC_DIR)
	$(CC) $(LFLAGS) $(LIBS) $(PERIODIC_OBJS) -o $(EXEC_DIR)/$(EXEC_P)

$(EXEC_NP) : $(NONPERIODIC_OBJS)
	@mkdir -p $(EXEC_DIR)
	$(CC) $(LFLAGS) $(LIBS) $(NONPERIODIC_OBJS) -o $(EXEC_DIR)/$(EXEC_NP)

$(PERIODIC_OBJS_DIR)/%.o : CFLAGS += -D$(PERIODIC_FLAG)
$(PERIODIC_OBJS_DIR)/%.o : %.cpp
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) $< -o $@
$(NONPERIODIC_OBJS_DIR)/%.o : %.cpp
	@mkdir -p $(@D)
	$(CC) -c $(CFLAGS) $< -o $@

clean :
	rm -rf $(NONPERIODIC_OBJS_DIR) $(PERIODIC_OBJS_DIR) $(EXEC_DIR)


##########################################

$(addsuffix main.o, $(PERIODIC_OBJS_DIR)/ $(NONPERIODIC_OBJS_DIR)/) : \
	main.cpp globals.h dynamic_array.h three_d_grid.h particle_data.h proto.h default_error.h arginput.h
$(addsuffix arginput.o, $(PERIODIC_OBJS_DIR)/ $(NONPERIODIC_OBJS_DIR)/) : \
	arginput.cpp globals.h proto.h init.h input_argument.h arginput.h default_error.h
$(addsuffix init.o, $(PERIODIC_OBJS_DIR)/ $(NONPERIODIC_OBJS_DIR)/) : \
	init.cpp globals.h proto.h init.h
$(addsuffix G2_read.o, $(PERIODIC_OBJS_DIR)/ $(NONPERIODIC_OBJS_DIR)/) : \
	G2_read.cpp globals.h dynamic_array.h particle_data.h proto.h default_error.h
$(addsuffix transform.o, $(PERIODIC_OBJS_DIR)/ $(NONPERIODIC_OBJS_DIR)/) : \
	transform.cpp dynamic_array.h particle_data.h three_d_grid.h globals.h proto.h default_error.h
$(addsuffix grid.o, $(PERIODIC_OBJS_DIR)/ $(NONPERIODIC_OBJS_DIR)/) : \
	grid.cpp dynamic_array.h particle_data.h three_d_grid.h globals.h proto.h default_error.h
$(addsuffix kernels.o, $(PERIODIC_OBJS_DIR)/ $(NONPERIODIC_OBJS_DIR)/) : \
	kernels.cpp dynamic_array.h particle_data.h three_d_grid.h globals.h proto.h kernels.h default_error.h
