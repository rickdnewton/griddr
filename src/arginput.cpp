#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <stdexcept>
#include <string> // string

#include <cstdlib> // atoi atof

#include "globals.h"
#include "proto.h"
#include "input_argument.h"
#include "arginput.h"
#include "default_error.h"

using namespace std;

/**
    \file arginput.cpp
    An implementation cpp file containing the definitions of code parameter objects and some
    functions relavant to their handling and display.
*/

/////////////////////////
std::string input_file_location;
std::string output_file_location;
std::string param_file_location;

int input_format;
int output_format;

int use_vels;

double x_r;
double y_r;
double z_r;
double theta_r;
double phi_r;
double x_r0;
double y_r0;
double z_r0;

int NxDim;
int NyDim;
int NzDim;

int LimMode;
int grid_inc_type;

double xLen_g;
double yLen_g;
double zLen_g;
double x_g0;
double y_g0;
double z_g0;

double dx_grid;
double dy_grid;
double dz_grid;

double xLim_g[2];
double yLim_g[2];
double zLim_g[2];

int sph_kernel_type;

bool help_on = false;
/////////////////////////

static ArgumentList input_arguments;

//! Function in which all code input parameters are specified as members of an ArgumentList
/*!
  \sa GeneralInputArgument, ArgumentList
*/
void InitialiseArguments(void) {
  input_arguments.AppendArgument(
    GeneralInputArgument<bool>("print usage", "--help", &help_on, false, false, false) );

  input_arguments.AppendArgument(
    GeneralInputArgument<std::string>("parameter_file_location", "--p", &param_file_location, "", false) );
  input_arguments.AppendArgument(
    GeneralInputArgument<std::string>("input_file_location", "--i", &input_file_location, "") );
  input_arguments.AppendArgument(
    GeneralInputArgument<std::string>("output_file_location", "--o", &output_file_location, "out.csv") );

  input_arguments.AppendArgument(
    GeneralInputArgument<int>("input_format", "--iFormat", &input_format, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<int>("output_format", "--oFormat", &output_format, 0) );

  input_arguments.AppendArgument(
    GeneralInputArgument<int>("UseVels", "--vels", &use_vels, 0) );

  input_arguments.AppendArgument(
    GeneralInputArgument<double>("theta", "--theta", &theta_r, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("phi", "--phi", &phi_r, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("rot_x", "--rot_x", &x_r, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("rot_y", "--rot_y", &y_r, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("rot_z", "--rot_z", &z_r, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("x_0", "--rx0", &x_r0, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("y_0", "--ry0", &y_r0, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("z_0", "--rz0", &z_r0, 0) );

  input_arguments.AppendArgument(
    GeneralInputArgument<int>("NxDim", "--Nx", &NxDim, 256) );
  input_arguments.AppendArgument(
    GeneralInputArgument<int>("NyDim", "--Ny", &NyDim, 256) );
  input_arguments.AppendArgument(
    GeneralInputArgument<int>("NzDim", "--Nz", &NzDim, 256) );

  input_arguments.AppendArgument(
    GeneralInputArgument<int>("LimMode", "--lMode", &LimMode, 0) );

  input_arguments.AppendArgument(
    GeneralInputArgument<int>("inc_type", "--gType", &grid_inc_type, 0) );

  input_arguments.AppendArgument(
    GeneralInputArgument<double>("xMin", "--xMin", &xLim_g[0], 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("xMax", "--xMax", &xLim_g[1], 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("yMin", "--yMin", &yLim_g[0], 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("yMax", "--yMax", &yLim_g[1], 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("zMin", "--zMin", &zLim_g[0], 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("zMax", "--zMax", &zLim_g[1], 0) );

  input_arguments.AppendArgument(
    GeneralInputArgument<double>("xLen", "--gdx", &xLen_g, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("yLen", "--gdy", &yLen_g, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("zLen", "--gdz", &zLen_g, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("xCentre", "--gx0", &x_g0, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("yCentre", "--gy0", &y_g0, 0) );
  input_arguments.AppendArgument(
    GeneralInputArgument<double>("zCentre", "--gz0", &z_g0, 0) );

  input_arguments.AppendArgument(
    GeneralInputArgument<int>("sph_kernel", "--K", &sph_kernel_type, 0) );

  return;
}

//! Function which searches a parameter file for all parameters expected from the ArgumentList and reads their values
/*!
  \param parameter_file_location path to target parameter file
  \sa ArgumentList
*/
void ParameterFileRead(const char *parameter_file_location) {

  int max_num_in = 2000;            //!< maximum number of parameterfile iterations (catches infinite loops)
  string buff, variable_label, var; //!< temporary strings to hold a buffer, variable labels and variable values

  ifstream parameter_file_stream(parameter_file_location);
  if (parameter_file_stream.is_open()) {
    cout << "Reading parameter file: " << parameter_file_location << endl;

    int it=0;
    while(parameter_file_stream) {
      parameter_file_stream >> buff;
      if (!parameter_file_stream)
        break;

      if (buff == "//!") {        // lines to be read will begin with this pattern
        parameter_file_stream >> variable_label;
        size_t i = 0;
        while (i < input_arguments.Length()) {
          if(variable_label == input_arguments[i]->description()) { // match to known input variables
            if(input_arguments[i]->file_argument()) {               // if matched variable is defined in the parameter file
              parameter_file_stream >> var;                         // ...read the variable value
              if(!input_arguments[i]->found_in_terminal())          // do not overwrite if value was set via terminal
                input_arguments[i]->set_target_variable(var);       // otherwise set value
            } else {
              input_arguments[i]->set_target_variable();            // no value to read, set as True
            }
            break;
          }
          ++i;
        }

        if (i >= input_arguments.Length())
          cerr << "WARNING: Parameter read failed for:" << "\n\t" << variable_label << endl;

      }
      it++;
      if(it > max_num_in) {
        parameter_file_stream.close();
        throw FileAccessError("ERROR: Parameter file stuck");
      }
    }
  } else {
    throw FileOpenError("Unable to open parameter file");
  }
  parameter_file_stream.close();

  return;
}

//! Function which searches a the command line input for all parameters expected from the ArgumentList and reads their values
/*!
  \param argc number of terminal arguments
  \param argv array of terminal arguments as constant character arrays (c strings)
  \sa ArgumentList
*/
int param_cmd_parse(int argc, char *argv[]) {
  size_t it = 0;
  while (it < argc) {
    string variable_flag = argv[it++];

    size_t i = 0;
    while (i < input_arguments.Length()) {
      if(variable_flag == input_arguments[i]->flag()) {
        input_arguments[i]->set_found_in_terminal(true);
        if(input_arguments[i]->terminal_argument()) {
          string var;
          if (it < argc) {
            var = argv[it++];
          } else {
              throw DefaultError("Missing command line argument corresponding to flag");
          }
          input_arguments[i]->set_target_variable(var);
        } else {
          input_arguments[i]->set_target_variable();
        }
        break;
      }
      ++i;
    }
    if (i >= input_arguments.Length())
      cerr << "WARNING: Parameter parsing failed for:" << "\n\t" << variable_flag << endl;
  }


  if ((argc == 0) || (help_on)) {
    PrintUsage();
    return 1;
  }

  return 0;
}

//! Function which loops over the ArgumentList and prints to screen all available arguments, flags and descriptions
void PrintUsage(void) {
  // find longes flag and description to neatly space/layout the output
  size_t max_flag_length = 0;
  size_t max_description_length = 0;
  for (vector<BaseInputArgument*>::iterator it = input_arguments.Begin() ; it != input_arguments.End(); ++it) {
    max_flag_length = (string((*it)->flag()).length() > max_flag_length) ? string((*it)->flag()).length() : max_flag_length;
    max_description_length = (string((*it)->description()).length() > max_description_length) ? string((*it)->description()).length() : max_description_length;
  }

  cout << "   Code usage:\n" << endl;
  cout << "\t" << "Flag:";
  for(size_t i = 0; i < max_flag_length+2; ++i) cout << " ";
  cout << "Description:" << endl;

  cout << "\t";
  for(size_t i = 0; i < max_flag_length+max_description_length+9; ++i) cout << "-";
  cout << endl;

  // output the flag an description for all available command-line arguments
  for (vector<BaseInputArgument*>::iterator it = input_arguments.Begin() ; it != input_arguments.End(); ++it)
    cout << "\t" << setw(max_flag_length+2) << (*it)->flag() << ":    " << (*it)->description() << endl;

  cout << endl;

  return;
}
