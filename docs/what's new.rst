What's New
==========

What's New in Griddr v0.7
-------------------------

- New code layout

  - All source files now in :code:`src/`
  - All docs in :code:`docs/`
  - Compiled code produces binaries in :code:`bin/`
