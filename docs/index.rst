.. Griddr documentation master file, created by
   sphinx-quickstart on Mon Oct 20 15:26:25 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Griddr's documentation!
==================================

.. toctree::
   :maxdepth: 2

   intro
   what's new
   setup
   tutorial
   code structure
   useful links
