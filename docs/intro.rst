Introduction
============

Griddr is a utility to efficiently smooth SPH (Smoothed Particle Hydrodynamics)
particle data onto regular 2 and 3D grids.

If you wanted to make an image of a cube centred on the origin with side length
10 you would simply have to write:

.. code-block:: bash

   ./Griddr-P --p ParameterFile --gdx 10 --gdy 10 --gdz 10

For an in-depth guide to installing Griddr, see :ref:`installation_reference`.

Features
--------

- Reads Gadget2 binary and `HDF5 <http://www.hdfgroup.org/HDF5/>`_ file formats
- Smooths any data discovered in a `HDF5 <http://www.hdfgroup.org/HDF5/>`_ file
- Accurately calculates SPH field values at the position of each pixel
- Multiple smoothing kernels available
- Outputs data as .csv files (more formats on the way)
- Python wrapper available
- Optionally accounts for perspective and dome/fisheye projections
- Some use of `OpenMP <http://openmp.org/>`_ acceleration

Contribute
----------

- Issue Tracker: `bitbucket.org/rickdnewton/griddr/issues <http://bitbucket.org/rickdnewton/griddr/issues>`_
- Source Code: `bitbucket.org/rickdnewton/griddr/issues <http://bitbucket.org/rickdnewton/griddr>`_

Support
-------

If you are having issues, please let us know by e-mailing richard.newton@uwa.edu.au
