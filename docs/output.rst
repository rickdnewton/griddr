3. Output
=========

Currently Griddr outputs data only as comma-separated-values (.csv) files with
the following format:

.. code-block:: none

   (0)x coord, (1)y coord, (2)z coord, (3)N_i, (4)N_i+1, ..., (x)Projected property 1, (x+1)Projected property 2, ...

Where the first three columns are the coordinates of the grid cell centres, the
following columns :math:`N_i` indicate the weight contribution from particles of type :math:`i`,
and the subsequent columns are the line-of-sight integral of the gridded property.
