2. Defining a Griddr Run
========================

Griddr can take its inputs from both a parameter file and though the terminal.

When defining the parameters for a run (see ) Griddr can either read from a parameter
file:

.. code-block:: bash

   ./Griddr-P --p parameters.param

take them from the terminal:

.. code-block:: bash

   ./Griddr-P --i inputFile.G2 --gdx 10 --gdy 10 --gdz 10 --o outFile.csv

or use a combination of both:

.. code-block:: bash

   ./Griddr-P --p parameters.param --i inputFile2.G2 --theta 45

in this instance, the terminal values override.

2.1 The Parameter File
----------------------

For a complete example parameter file ('parameters.param') see below:

.. include:: ../parameters.param
   :literal:


2.2 Via the Command Line
------------------------

A complete list of the current parameters can be obtained by calling Griddr with
no arguments or with e.g.

.. code-block:: bash

   ./Griddr-P --help
