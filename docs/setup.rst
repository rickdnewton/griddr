Setup
=====

.. _installation_reference:

Installation
------------

Install Griddr by first obtaining the code from the public repository at
`Bitbucket <https://bitbucket.org/rickdnewton/griddr>`_ using `git <http://git-scm.com/>`_
by opening a terminal in the location you want the :code:`griddr` directory
created and executing

.. code-block:: bash

   git clone https://rickdnewton@bitbucket.org/rickdnewton/griddr.git

then you obtain `HDF5 <http://www.hdfgroup.org/HDF5/>`_ (necessary for reading some
simulation files) and set its location in the Makefile (as well as defining your
compiler), then simply

.. code-block:: bash

   make

to produce two binary files, one for use with periodic simulations, 'Griddr-P',
and one for non-periodic, 'Griddr-NP'.


Mac installation guide
^^^^^^^^^^^^^^^^^^^^^^

If you are installing griddr on a Mac computer then additional dependencies must
be obtained. We recommend the use of `MacPorts <https://www.macports.org/>`_ for
easily obtaining the necessary C/C++ libraries, and
`Anaconda <https://store.continuum.io/cshop/anaconda/>`_ for the Python dependencies,
although other alternatives exist. The installation and use of Griddr requires the
use of a Terminal instance.

.. note:: You should have `Xcode <https://developer.apple.com/xcode/>`_ installed to make system libraries and compilers
   available when using programs provided as source code and for installing many codes through `MacPorts <https://www.macports.org/>`_.

   - Install `Xcode <https://developer.apple.com/xcode/>`_ through the App Store
   - Install the `Xcode <https://developer.apple.com/xcode/>`_ command line tools via the terminal with :code:`xcode-select --install`
   - If you have not already done so, accept the `Xcode <https://developer.apple.com/xcode/>`_ license with :code:`xcodebuild -license`

1. Install `MacPorts <https://www.macports.org/>`_

   - using the installer which can be obtained from `here <https://guide.macports.org/#installing.macports>`_

2. Install `HDF5 <http://www.hdfgroup.org/HDF5/>`_ libraries

   - via the terminal using MacPorts by entering :code:`sudo port install hdf5`

3. Compile Griddr

   - Open a terminal in the Griddr :code:`src/` directory and enter :code:`make` to produce the code binaries in :code:`bin/`

If you wish to use the Python wrapper for Griddr or the bundled utilities for image generation then you may need to install Python; in this case

4. Install `Anaconda <https://store.continuum.io/cshop/anaconda/>`_

   - using the installer which can be obtained from `here <https://store.continuum.io/cshop/anaconda/>`_
   - ensure you have the most up-to-date version by running :code:`conda update conda`

5. Install `iPython <http://ipython.org/>`_ to give you the ability to interactively work with the data and `Matplotlib <http://http://matplotlib.org/>`_ to allow the plotting and image generation

   - :code:`conda install ipython`
   - :code:`conda install matplotlib`

Choice of Compiler
^^^^^^^^^^^^^^^^^^

When using any C/C++ software distributed as source code, it is necessary to make
use of a compiler. The compiler takes the text codes written by the programmer and
turns them in to a single executable file, or *binary*, which is then run to begin
the program. Installing Griddr using `GNU C++ compiler <https://gcc.gnu.org/>`_ (`g++ <https://gcc.gnu.org/>`_)
should be relatively painless on Linux, however Modern Mac OS does not ship with the
`g++ <https://gcc.gnu.org/>`_ installed as standard, instead using it as an alias
for the `LLVM clang compiler <http://clang.llvm.org/>`_.

Either compiler can be used in principle, however at the time of writing `clang <http://clang.llvm.org/>`_ does
not support multithreading through `OpenMP <http://openmp.org/>`_ which has some limited use in Griddr.

In order to use `clang <http://clang.llvm.org/>`_, it is necessary to change two lines in the Makefile,
the relevant lines chould be changed to

.. code-block:: make

   CC = clang -std=c++11 -stdlib=libc++

   ...

   LIBS = $(HDF5_LIBS) -lm  -lc++

If instead you wish to install and use `g++ <https://gcc.gnu.org/>`_ you may simply
install it via `MacPorts <https://www.macports.org/>`_ with, for example

.. code-block:: bash

   sudo port install gcc49

after which you can either use :code:`gcc-mp-4.9` as your compiler in the Makefile,
or set the gcc alias to point to the `GNU compiler <https://gcc.gnu.org/>`_ instead
of `clang <http://clang.llvm.org/>`_ with

.. code-block:: bash

   sudo port select --set gcc mp-gcc49

where you can see which compilers are available by entering :code:`port select --list gcc`.

Updating the Code
-----------------

Updated versions of the code will be pushed periodically to the
`Bitbucket <https://bitbucket.org/rickdnewton/griddr>`_ repository. To take advantage
of these updates and bug-fixes, you open a terminal in the :code:`griddr/` parent
directory, then pull the updated code onto your machine with

.. code-block:: bash

   git pull

before moving into the source code directory :code:`cd src/` and re-building the code

.. code-block:: bash

   make clean
   make


Additional Makefile options
---------------------------

The code contains various preprocessor commands to change its default behaviour.
By activating (i.e. uncommenting, removing the :code:`#` symbol) any of the
following options in the Makefile and re-compiling the code (e.g. with
:code:`make clean; make`), we can get different products from the code.

To compile the code in a version which produces three dimensional grids rather than two:

.. code-block:: make

   OPT += -DTHREE_DIMS

The code may be instructed to emply a simplifying assumption in the gridding
algorithm whereby all particles are assumed to lie at the centres of their cells,
meaning that tabulated radius tables can be used. To activate this mode use:

.. code-block:: make

   OPT += -DFAST_PLOT

The following options are only valid if used with 2D plotting. To shift from an isometric
to a perspective projection (with opening angle 60°) activate:

.. code-block:: make

   OPT += -DPERSPECTIVE

In order to modify the perspective calculation to produce a fish-eye style image, additionally activate:

.. code-block:: make

   OPT += -DFISHEYE

For use only with both perspective and fish-eye calculations, to shift the focus
of the image to a declination of 60° (to appear at eye-level in the Scitech planetarium) use:

.. code-block:: make

   OPT += -DSCITECH_TILT
