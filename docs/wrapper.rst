4. The Python Wrapper
=====================

Rather than outputting to the disk, Griddr can be called via python and return the
projected data in memory. This means that the processing time in an image-making pipeline
is reduced at the expense of no permanent record of the gridded data on disk.
