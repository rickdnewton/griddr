1. Run-Time Parameters
======================

In addition to simple parameters such as the locations of the input and output files,
Griddr can take many other parameters. These range from the data formats, defining
geometrical transforms to be performed on the data prior to gridding, and the nature
of the grid itself.

Main Run Parameters
-------------------

The primary configuration options which must be set for the run are:
- Input file location
- Input & output file formats
- Whether or not to calculate mean values of the :math:`x`, :math:`y`, :math:`z` components of velocity (with
axes defined by the projection grid)

Transformation Parameters
-------------------------

The simulation data loaded in to Griddr may be re-centred and rotated (about its
new origin if re-centred).

If periodic boundary conditions are in use (Griddr-P) then all translating the system
will result in the data being wrapped back in to a cube about the new origin (coordinates
spanning from :math:`0` -> :math:`L` in each direction rather than :math:`-L/2` -> :math:`L/2`).

Rotations may be defined either though the use of spherical coordinate rotations
using :math:`\theta` and :math:`\phi`, or as rotations about the three cartesian axes as defined by
the projection grid.

Grid Parameters
---------------

Once all transformations have taken place, the particle data is then projected onto a grid which may be defined in any of three possible ways

Component Gridding Parameters
-----------------------------

It is currently not possible to choose specific datasets to project onto the grid
with Griddr. This is due to the way Griddr automatically discovers and grids all
data associated with a particle type with defined smoothing lengths; without a GUI
it is unwieldy to prompt the user for a choice on which datasets to ignore.

Default Parameters
------------------

To simplify running griddr to produce a quick image via the command line, sevaral
parameters have default values which are overriden by the user. For example, by
default Griddr will attempt to choose grid limits which enclose all gas particles.


+---------------------------------------------+----------------------------------------+
| Parameter                                   | Default Value                          |
+=============================================+========================================+
| Output file name                            | out.csv                                |
+---------------------------------------------+----------------------------------------+
| Input format                                | 0                                      |
+---------------------------------------------+----------------------------------------+
| Output format                               | 0                                      |
+---------------------------------------------+----------------------------------------+
| Ncells x-dimension                          | 256                                    |
+---------------------------------------------+----------------------------------------+
| Ncells y-dimension                          | 256                                    |
+---------------------------------------------+----------------------------------------+
| Ncells z-dimension (if active)              | 256                                    |
+---------------------------------------------+----------------------------------------+
| Grid limits mode                            | 0                                      |
+---------------------------------------------+----------------------------------------+
| Grid limits include type                    | 0                                      |
+---------------------------------------------+----------------------------------------+
| SPH kernel                                  | 0                                      |
+---------------------------------------------+----------------------------------------+
